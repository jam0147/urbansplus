<?php

namespace App\Http\ViewComposers;

use ExportLocalization;
use Illuminate\View\View;

class LocalizationComposer
{
    /**
     * @param View $view
     * @return mixed
     */
    public function compose(View $view)
    {
        return $view->with( [
            'localizations' => ExportLocalization::export()->toArray(),
        ] );
    }
}
