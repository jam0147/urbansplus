<nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li class="nav-item mobile-menu hidden-md-up float-xs-left">
                    <a class="nav-link nav-menu-main menu-toggle hidden-xs">
                        <i class="icon-menu5 font-large-1"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}" class="navbar-brand nav-link">
                        <img alt="branding logo" src="{{ config('app.logo') }}" data-expand="{{ asset(config('app.logo')) }}"
                            data-collapse="{{ asset(config('app.logo-small')) }}" class="brand-logo">
                    </a>
                </li>
                <li class="nav-item hidden-md-up float-xs-right">
                    <a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container">
                        <i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content container-fluid">
            <div id="navbar-mobile" class="collapse navbar-toggleable-sm">


                <ul class="nav navbar-nav">

                    <li class="nav-item hidden-sm-down">
                        <a class="nav-link nav-menu-main menu-toggle hidden-xs">
                            <i class="icon-menu5"></i>
                        </a>
                    </li>

                </ul>

                <ul class="nav navbar-nav float-xs-right">

                    <li class="dropdown dropdown-language nav-item">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-language-link">
                            <span class="avatar">
                                <img src="{{ asset('images/icons/world.png') }}">
                            </span>
                            <span class="language">
                              @lang('navbar.language')
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{ route('lang', ['lang' => 'es']) }}" class="dropdown-item">
                                @lang('navbar.languages.spanish')
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('lang', ['lang' => 'en']) }}" class="dropdown-item">
                                @lang('navbar.languages.english')
                            </a>
                        </div>
                    </li>

                    <li class="dropdown dropdown-user nav-item">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                            <span class="avatar">
                                <img src="{{ asset('images/avatars') }}/{{ Auth::user()->avatar }}" alt="avatar">
                            </span>
                            <span class="user-name">{{ Auth::user()->name }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{ route('profile', Auth::user()->id) }}" class="dropdown-item">
                                <i class="icon-head"></i>
                                @lang('navbar.my-profile')
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('lock') }}" class="dropdown-item">
                                <i class="icon-lock4"></i>
                                @lang('navbar.lock-account')
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                <i class="icon-power3"></i>
                                @lang('navbar.logout')
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
