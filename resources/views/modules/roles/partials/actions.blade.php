@can('roles.show')
    <a href="{{ route('roles.show', $id) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="@lang('modules.roles.roles-show')">
        <i class="icon-eye"></i>
    </a>
@endcan

@can('roles.edit')
    <a href="{{ route('roles.edit', $id) }}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="@lang('modules.roles.roles-edit')">
        <i class="icon-edit"></i>
    </a>
@endcan

@can('roles.destroy')
    <button type="button" class="btn btn-danger" onclick="destroy({{ $id }})" data-toggle="tooltip" data-placement="top" title="@lang('modules.roles.roles-destroy')">
        <i class="icon-trash"></i>
    </button>
@endcan
