@extends('layouts.app')

@section('page_title', __("modules.users.users-show") ." ". $user->name)

@section('breadcrumbs')
    {{ Breadcrumbs::render('users.show', $user) }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title text-uppercase" id="basic-layout-card-center">
                        @lang('modules.users.users-show')
                    </h4>
                </div>
                <div class="card-body">
                    <div class="card-block">
                        <form class="form">
                            <div class="row">
                                <div class="col-md-6 offset-md-3">
                                    <div class="form-body">

                                        <div class="form-group">
                                            {{ html()->label(__('modules.users.users-form.name')) }}
                                            {{ html()->input('text')->class('form-control')->value($user->name)->disabled() }}
                                        </div>

                                        <div class="form-group">
                                            {{ html()->label(__('modules.users.users-form.email')) }}
                                            {{ html()->input('email')->class('form-control')->value($user->email)->disabled() }}
                                        </div>

                                        @if(!$user->getRoleNames()->first() == '')
                                            <div class="form-group">
                                                {{ html()->label(__('modules.users.users-form.role')) }}
                                                {{ html()->input('text')->class('form-control')->value($user->getRoleNames()->first())->disabled() }}
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="form-actions center">
                                <a href="{{ route('users.index') }}" class="btn btn-warning mr-1">
                                    <i class="icon-arrow-left4"></i>
                                    @lang('modules.users.users-form.go-back')
                                </a>

                                @can('users.edit')
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary mr-1">
                                        <i class="icon-edit"></i>
                                        @lang('modules.users.users-edit')
                                    </a>
                                @endcan

                                @can('users.destroy')
                                <button class="btn btn-danger" type="button" onclick="deleteUser({{ $user->id }})">
                                    <i class="icon-trash"></i>
                                    @lang('modules.users.users-destroy')
                                </button>
                                @endcan

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">

        function deleteUser(id)
        {
            swal({
                title: "@lang('modules.users.users-messages.are-you-sure')",
                text: "@lang('modules.users.users-messages.text-confirm-delete')",
                icon: 'warning',
                buttons: {
                    cancel: {
                        text: "@lang('modules.users.users-messages.cancel')",
                        value: false,
                        visible: true,
                        className: "btn btn-danger mr-1",
                        closeModal: true
                    },
                    confirm: {
                        text: "@lang('modules.users.users-messages.confirm-delete')",
                        value: true,
                        visible: true,
                        className: "btn btn-success",
                        closeModal: true
                    }
                },
            }).then((result) => {

                if (result == true) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "DELETE",
                        url: "/admin/users/" + id,
                        dataType: "JSON",
                    }).success(function (response) {
                        swal({
                            icon: 'success',
                            title: response.title,
                            text: response.message
                        });
                        setTimeout(function (){
                            location.href ="{{ route('users.index') }}";
                        }, 2000);
                    }).error(function (response) {
                        console.log(response);
                        swal({
                            icon: 'error',
                            title: response.responseJSON.title,
                            text: response.responseJSON.message
                        });
                    });
                } else if (result === false) {
                    swal({
                        title: "@lang('modules.users.users-messages.canceled')",
                        text: "@lang('modules.users.users-messages.no-deleted')",
                        icon: "error"
                    });
                }
            });
        }

    </script>
@endpush
