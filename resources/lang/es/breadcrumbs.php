<?php

return [
    'dashboard' => 'Tablero',
    'users' => [
        'index' => 'Usuarios',
        'create' => 'Crear usuario',
        'details' => 'Detalles',
        'edit' => 'Editar',
    ],
    'roles' => [
        'index' => 'Roles',
        'create' => 'Crear rol',
        'details' => 'detalles',
        'edit' => 'Editar',
    ],
    'permissions' => 'Permisos',
];
