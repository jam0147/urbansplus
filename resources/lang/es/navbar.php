<?php

return [
    'language' => 'Idioma',
    'languages' => [
        'spanish' => 'Español',
        'english' => 'Ingles',
    ],
    'lock-account' => 'Bloquear cuenta',
    'logout' => 'Cerrar sesión',
    'my-profile' => 'Mi perfil',
];
