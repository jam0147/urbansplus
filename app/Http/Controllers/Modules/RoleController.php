<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleStoreRequest;
use App\Http\Requests\RoleUpdateRequest;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DataTables;
use App\Helpers\RolesHelpers;

class RoleController extends Controller
{
    use RolesHelpers;

    /**
     * RoleController constructor.
     */
    public function __construct()
    {
        $this->middleware([
            'permission:roles.index',
            'permission:roles.create',
            'permission:roles.edit',
            'permission:roles.destroy',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.roles.index');
    }

    /**
     * Display Response Json data of te roles.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        $role = Role::query();

        return DataTables::eloquent($role)
            ->addColumn('actions', 'modules.roles.partials.actions')
            ->addColumn('created', 'modules.roles.partials.created_at')
            ->rawColumns(['actions', 'created'])
            ->toJson();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function permisionsRoles($id)
    {
        $role = Role::findById($id);

        $permissions = $role->permissions()->get();

        return DataTables::of($permissions)
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all()
            ->pluck('display_name', 'name');

        return view('modules.roles.create', [ 'permissions' => $permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RoleStoreRequest $request)
    {
        return $this->createRole($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return view('modules.roles.show', ['role' => $role]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $permissions = Permission::all()
            ->pluck('display_name', 'name');

        $permissions_selected =  $role->permissions
            ->pluck('name')
            ->toArray();

        return view('modules.roles.edit', [ 'permissions' => $permissions, 'permissions_selected' => $permissions_selected, 'role' => $role]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(RoleUpdateRequest $request, Role $role)
    {
        return $this->updateRole($request, $role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Role $role)
    {
        switch ($role) {
            case $role->name == "administrator" || $role->name == "user":
                return $this->defaultRolesResponse();
                break;
            default:
                return $this->deleteRole($role);
                break;
        }
    }
}
