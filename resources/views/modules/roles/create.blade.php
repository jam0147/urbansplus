@extends('layouts.app')

@section('page_title', __('modules.roles.roles-form.create-role'))

@section('breadcrumbs')
    {{ Breadcrumbs::render('roles.create') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title float-lg-left text-uppercase">
                        @lang('modules.roles.roles-form.roles-form-creation')
                    </h4>
                </div>
                <div class="card-body">
                    <div class="card-block">
                        @include('modules.roles.partials.form', ['id' => 'create-role-form', 'btn_text' => __('modules.roles.roles-form.save')])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')

    <script type="text/javascript">

        $("#create-role-form").on('submit', function (event) {
            event.preventDefault();

            $("#submit-button").attr("disabled", true);
            $("#name").removeClass('border-red');
            $("#description").removeClass('border-red');
            $("#permissions").removeClass('border-red');
            $("#error_name").addClass('display-hidden').text("");
            $("#error_description").addClass('display-hidden').text("");
            $("#error_permissions").addClass('display-hidden').text("");

            let formData = {
                name: $("#name").val(),
                description: $("#description").val(),
                permissions: $("#permissions").val()
            };

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('roles.store') }}",
                type: "POST",
                dataType: "JSON",
                data: {
                    name: formData.name,
                    description: formData.description,
                    permissions: formData.permissions
                }
            }).success(function (response) {
                $("#submit-button").attr("disabled", false);
                $("#create-role-form")[0].reset();
                $('#permissions').val('').trigger('change');
                console.log(response);
                swal({
                    icon: 'success',
                    title: response.title,
                    text: response.message
                });
            }).error(function (response) {
                $("#submit-button").attr("disabled", false);
                console.error(response);
                swal({
                    icon: 'error',
                    title: response.responseJSON.message,
                    text: response.statusText
                });

                let error_name = response.responseJSON.errors.name;
                let error_description = response.responseJSON.errors.description;
                let error_permissions = response.responseJSON.errors.permissions;

                if (error_name) {
                    console.error(error_name.toString());
                    $("#name").addClass('border-red');
                    $("#error_name").removeClass('display-hidden').text(error_name.toString());
                }

                if (error_description) {
                    console.error(error_description.toString());
                    $("#description").addClass('border-red');
                    $("#error_description").removeClass('display-hidden').text(error_description.toString());
                }

                if (error_permissions) {
                    console.error(error_permissions.toString());
                    $("#permissions").addClass('border-red');
                    $("#error_permissions").removeClass('display-hidden').text(error_permissions.toString());
                }

            })

        })

    </script>
@endpush
