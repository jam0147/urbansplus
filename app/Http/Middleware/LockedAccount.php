<?php

namespace App\Http\Middleware;

use Closure;

class LockedAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('locked')) {

            return redirect()->route('lock');

        }


        return $next($request);
    }
}
