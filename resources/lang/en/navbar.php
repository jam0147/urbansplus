<?php

return [
    'language' => 'Language',
    'languages' => [
        'spanish' => 'Spanish',
        'english' => 'English',
    ],
    'lock-account' => 'Lock account',
    'logout' => 'Logout',
    'my-profile' => 'My profile',
];
