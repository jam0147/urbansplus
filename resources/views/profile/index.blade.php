@extends('layouts.app')

@section('page_title', __('modules.profile.profile-header'))

@section('breadcrumbs')
 {{ Breadcrumbs::render('profile', $user) }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title text-uppercase" id="basic-layout-card-center">
                        @lang('modules.profile.profile-title')
                    </h4>
                </div>
                <div class="card-body">
                    <div class="card-block">

                        {!! Form::model($user, ['route' => ['profile.change-password', $user->id], 'class' => 'form', 'method' => 'put']) !!}
                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <div class="form-body">

                                    <div class="form-group">
                                        {!! Form::label(__('modules.profile.form.name')) !!}
                                        {!! Form::text('name', $user->name, ['class' => 'form-control', 'disabled']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label(__('modules.profile.form.email')) !!}
                                        {!! Form::email('email', $user->email, ['class' => 'form-control', 'disabled']) !!}
                                    </div>

                                    @if(!$user->getRoleNames()->first() == '')
                                        <div class="form-group">
                                            {{ html()->label(__('modules.profile.form.role')) }}
                                            {{ html()->input('text')->class('form-control')->value($user->getRoleNames()->first())->disabled() }}
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        {!! Form::label(__('modules.profile.form.change-password')) !!}
                                        {!! Form::password('password', ['class' => 'form-control']) !!}
                                        @if ($errors->has('password'))
                                            <span class="text-danger">
                                                    {{ $errors->first('password') }}
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions center">
                            <a href="{{ route('dashboard') }}" class="btn btn-warning mr-1">
                                <i class="icon-arrow-left4"></i>
                                @lang('modules.profile.form.go-back')
                            </a>
                            <button type="submit" class="btn btn-primary mr-1">
                                <i class="icon-save"></i>
                                @lang('modules.profile.form.save-changes')
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
