@extends('layouts.auth')

@section('page_title', __('auth.login'))

@section('content')
    <div class="card border-grey border-lighten-3 m-0">
        <div class="card-header no-border">
            <div class="card-title text-xs-center">
                <div class="p-1">
                    <img src="{{ asset('images/logo/logo-dark.png') }}" alt="branding logo">
                </div>
            </div>
            <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2">
                <span>@lang('auth.login-in-system')</span>
            </h6>
        </div>
        <div class="card-body collapse in">
            <div class="card-block">
                <form class="form-horizontal form-simple" action="{{ route('login') }}" method="POST" novalidate>
                    @csrf

                    <fieldset class="form-group position-relative has-icon-left mb-1">
                        <input type="email" name="email" value="{{ old('email') }}" class="form-control form-control-lg input-lg {{ $errors->has('email') ? 'border-danger' : '' }}" placeholder="@lang('auth.enter-email')" required autofocus>
                        <div class="form-control-position">
                            <i class="icon-head"></i>
                        </div>
                        @if ($errors->has('email'))
                            <span class="text-danger">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </fieldset>

                    <fieldset class="form-group position-relative has-icon-left">
                        <input type="password" name="password" class="form-control form-control-lg input-lg {{ $errors->has('password') ? 'border-danger' : '' }}" placeholder="@lang('auth.enter-password')" required>
                        <div class="form-control-position">
                            <i class="icon-key3"></i>
                        </div>
                        @if ($errors->has('password'))
                            <span class="text-danger">
                                {{ $errors->first('password') }}
                            </span>
                        @endif
                    </fieldset>

                    <fieldset class="form-group row">
                        <div class="col-md-6 col-xs-12 text-xs-center text-md-left">
                            <fieldset>
                                <input type="checkbox" class="chk-remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label>@lang('auth.remember-me')</label>
                            </fieldset>
                        </div>
                        <div class="col-md-6 col-xs-12 text-xs-center text-md-right">
                            <a href="{{ route('password.request') }}" class="card-link">
                                @lang('auth.forgot-password')
                            </a>
                        </div>
                    </fieldset>

                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                        <i class="icon-unlock2"></i> @lang('auth.login')
                    </button>

                </form>
            </div>
        </div>
    </div>
@endsection
