<footer class="footer footer-static fixed-bottom footer-light navbar-border">
    <p class="clearfix text-muted text-center mb-0 px-2">
        <span class="d-xs-block d-md-inline-block">
            Copyright  &copy; {{ date('Y') }} {{ config('author.name') }} , @lang('copyright.all-rights-reserved'),
            Powered by {{ config('author.powered-by') }}
        </span>
    </p>
</footer>
