<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|min:6',
            'email'         => 'required|min:6|email|unique:users,id, ' . $this->get('id'),
            'role'          => 'required',
        ];
    }

    public function messages()
    {
      return [
          'name.required'       => __("validation.users.update.name-required"),
          'name.min'            => __("validation.users.update.name-min"),
          'name.alpha'          => __("validation.users.update.name-alphanumeric"),
          'email.required'      => __("validation.users.update.email-required"),
          'email.min'           => __("validation.users.update.email-min"),
          'email.unique'        => __("validation.users.update.email-unique"),
          'email.email'         => __('validation.users.update.email-email'),
          'role.required'       => __('validation.users.update.rol-required')
      ];
    }
}
