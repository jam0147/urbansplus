<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
   	protected $table = "chat";
    protected $fillable = [
        'viaje_id', 'user_id', 'mensaje'
    ];

    protected $dates = ['created_at', 'updated_at'];
}
