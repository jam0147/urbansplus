<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajesHistorico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajes_historicos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('viaje_id');
            $table->unsignedInteger('viaje_etapa_id');
            $table->string('obs_pasajero', 200)->nullable();
            $table->string('obs_conductor', 200)->nullable();
            $table->foreign('viaje_id')->references('id')->on('viajes')->onDelete('cascade');
            $table->foreign('viaje_etapa_id')->references('id')->on('viajes_etapas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viajes_historicos');
    }
}
