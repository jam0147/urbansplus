<?php

return [
    'dashboard' => 'Tablero',
    'modules' => 'Módulos',
    'users-management' => 'Gestión de usuarios',
    'users' => 'Usuarios',
    'permissions' => 'Permisos',
    'roles' => 'Roles',
    'main-navigation' => 'Navegación principal'
];
