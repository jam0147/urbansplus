<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login' => 'Login',
    'remember-me' => 'Remember me',
    'forgot-password' => 'Forgot Password?',
    'enter-email' => 'Enter your email address',
    'enter-password' => 'Enter password',
    'confirm-password' => 'Confirm your password',
    'send-link' => 'send link by email',
    'login-in-system' => 'Log in to access the system',
    'send-email-description' => 'We will send you a link to reset your password.',
    'reset-password' => 'Reset Password',
    'unlock-account' => 'Unlock your account',
    'unlock'    => 'Unlock',
    'logout' => 'Logout',

];
