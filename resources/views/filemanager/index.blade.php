@extends('layouts.app')

@section('page_title', __('Administrador de archivos'))

@section('breadcrumbs')
    {{ Breadcrumbs::render('filemanager') }}
@endsection

@section('content')
    <iframe src="/admin/laravel-filemanager" style="width: 100%; height: 500px; overflow: hidden; border: none;"></iframe>
@endsection
