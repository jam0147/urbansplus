<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Viajes extends Model
{
    protected $table = "viajes";
    protected $fillable = [
        'pasajero_id', 'conductor_id', 'vehiculos_id', 'key_firebase', 'solicitud_tipo_vehiculo_id', 'inic_coord_inicio', 'inic_coord_final',
        'llegada_coord_inicio','llegada_coord_final', 'costo', 'calificacion_conductor', 'calificacion_pasajero', 'status', 'usuario_pagos_id', 'tipos_pagos_id', 'distancia_total_google', 'tiempo_total_google', 'msj_conductor', 'fecha_viaje', 'lugar_destino'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

}
