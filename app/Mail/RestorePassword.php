<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RestorePassword extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $clave;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $clave)
    {
        $this->user = $user;
        $this->clave = $clave;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.restore')
            ->from('lealcristian@gmail.com')
            ->subject('Admin urbansplus')
            ->with([
                'userName' => $this->user->name,
                'userPassword' => $this->clave,
                'userSurname' => $this->user->surname
            ]);

    }
}
