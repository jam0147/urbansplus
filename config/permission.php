<?php

return [

    'models' => [

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * Eloquent model should be used to retrieve your permissions. Of course, it
         * is often just the "Permission" model but you may use whatever you like.
         *
         * The model you want to use as a Permission model needs to implement the
         * `Spatie\Permission\Contracts\Permission` contract.
         */

        'permission' => Spatie\Permission\Models\Permission::class,

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * Eloquent model should be used to retrieve your roles. Of course, it
         * is often just the "Role" model but you may use whatever you like.
         *
         * The model you want to use as a Role model needs to implement the
         * `Spatie\Permission\Contracts\Role` contract.
         */

        'role' => Spatie\Permission\Models\Role::class,

    ],

    'table_names' => [

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your roles. We have chosen a basic
         * default value but you may easily change it to any table you like.
         */

        'roles' => 'roles',

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your permissions. We have chosen a basic
         * default value but you may easily change it to any table you like.
         */

        'permissions' => 'permissions',

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your models permissions. We have chosen a
         * basic default value but you may easily change it to any table you like.
         */

        'model_has_permissions' => 'model_has_permissions',

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your models roles. We have chosen a
         * basic default value but you may easily change it to any table you like.
         */

        'model_has_roles' => 'model_has_roles',

        /*
         * When using the "HasRoles" trait from this package, we need to know which
         * table should be used to retrieve your roles permissions. We have chosen a
         * basic default value but you may easily change it to any table you like.
         */

        'role_has_permissions' => 'role_has_permissions',
    ],

    'column_names' => [

        /*
         * Change this if you want to name the related model primary key other than
         * `model_id`.
         *
         * For example, this would be nice if your primary keys are all UUIDs. In
         * that case, name this `model_uuid`.
         */
        'model_morph_key' => 'model_id',
    ],

    /*
     * By default all permissions will be cached for 24 hours unless a permission or
     * role is updated. Then the cache will be flushed immediately.
     */

    'cache_expiration_time' => 60 * 24,

    /*
     * When set to true, the required permission/role names are added to the exception
     * message. This could be considered an information leak in some contexts, so
     * the default setting is false here for optimum safety.
     */

    'display_permission_in_exception' => true,

    /*
     * Set default roles and permissions.
     */
    'roles' => [

        'admin' => [
            'name' => 'administrator',
            'display_name' => 'Administrator',
            'description' => 'Role with total access to the system.'
        ],

        'pasajero' => [
            'name' => 'pasajero',
            'display_name' => 'Pasajero',
            'description' => 'Usuarios de tipo pasajero.'
        ],

        'conductor' => [
            'name' => 'conductor',
            'display_name' => 'Conductor',
            'description' => 'Usuarios de tipo conductor.'
        ]
    ],

    'permissions' => [
        'users-index' => [
            'name' => 'users.index',
            'display_name' => 'See user list',
            'description'   => 'Access to the list of users of the system'
        ],
        'users-show' => [
            'name' => 'users.show',
            'display_name' => 'See user details',
            'description'   => 'Access to the details of system users'
        ],
        'users-create' => [
            'name' => 'users.create',
            'display_name' => 'Create users',
            'description'   => 'Access to the user creation form'
        ],
        'users-edit' => [
            'name' => 'users.edit',
            'display_name' => 'Edit users',
            'description'   => 'Access to edit records of a system user'
        ],
        'users-destroy' => [
            'name' => 'users.destroy',
            'display_name' => 'Remove users',
            'description' => 'Access to remove system users'
        ],
        'roles-index' => [
            'name' => 'roles.index',
            'display_name' => 'See roles list',
            'description'   => 'Access to the list of roles of the system'
        ],
        'roles->show' => [
            'name' => 'roles.show',
            'display_name' => 'See roles details',
            'description'   => 'Access to the details of the roles'
        ],
        'roles-create' => [
            'name' => 'roles.create',
            'display_name' => 'Create roles',
            'description'   => 'Access to the roles creation form'
        ],
        'roles-edit' => [
            'name' => 'roles.edit',
            'display_name' => 'Edit roles',
            'description'   => 'Access to edit records of a system roles'
        ],
        'roles-destroy' => [
            'name' => 'roles.destroy',
            'display_name' => 'Remove roles',
            'description' => 'Access to remove system roles'
        ],
        'permissions-index' => [
            'name' => 'permissions.index',
            'display_name' => 'See permissions list',
            'description'   => 'Access to the list of system permissions'
        ]
    ],
];
