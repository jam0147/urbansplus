@extends('layouts.errors')

@section('page_title', __('Forbidden'))

@section('content')
    <div class="card-header bg-transparent no-border">
        <h2 class="error-code text-xs-center mb-2">403</h2>
        <h3 class="text-uppercase text-xs-center">{{ __('Forbidden') }}</h3>
    </div>
    <div class="card-body collapse in">
        <div class="row py-2">
            <div class="col-xs-12 col-sm-6 col-md-6 offset-md-3">
                <a href="{{ route('dashboard') }}" class="btn btn-primary btn-block font-small-3">
                    <i class="icon-home4"></i>
                    {{ __('Back to Home') }}
                </a>
            </div>
        </div>
    </div>
@endsection
