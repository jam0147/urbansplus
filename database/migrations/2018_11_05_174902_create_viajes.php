<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajes', function (Blueprint $table) {
            $table->increments('id');


            $table->unsignedInteger('pasajero_id');
            $table->unsignedInteger('conductor_id')->nullable();
            $table->unsignedInteger('vehiculo_id')->nullable();
            $table->unsignedInteger('usuario_pago_id')->nullable();
            $table->unsignedInteger('solicitud_tipo_vehiculo_id')->nullable();

            $table->unsignedInteger('tipos_pagos_id')->nullable();
            $table->string('inic_coord_inicio', 100)->nullable();
            $table->string('inic_coord_final', 100)->nullable();
            
            $table->string('llegada_coord_inicio', 100)->nullable();
            $table->string('llegada_coord_final', 100)->nullable();

            $table->string('distancia_total_google', 100)->nullable();
            $table->string('tiempo_total_google', 100)->nullable();
            $table->string('lugar_destino', 255)->nullable();
            
            $table->double('costo', 8, 2)->nullable();
            $table->integer('calificacion_conductor')->nullable();
            $table->integer('calificacion_pasajero')->nullable();
            $table->integer('status')->nullable();
            $table->string('key_firebase', 100)->nullable(); 
            $table->string('msj_conductor', 250)->nullable(); 

            $table->timestamp('fecha_viaje')->nullable(); 

            $table->foreign('pasajero_id')->references('id')->on('usuarios_pagos')->onDelete('cascade');

            $table->foreign('conductor_id')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('vehiculo_id')->references('id')->on('vehiculos')->onDelete('cascade');

            $table->foreign('usuario_pago_id')->references('id')->on('usuarios_pagos')->onDelete('cascade');

            $table->foreign('solicitud_tipo_vehiculo_id')->references('id')->on('usuarios_pagos')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viajes');
    }
}
