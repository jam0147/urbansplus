<?php

namespace App\Http\Controllers\Modules;

use App\Helpers\UsersHelpers;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Pais;
use App\Models\User;
use App\Models\TipoVehiculos;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use DataTables;

class UserController extends Controller
{
    use UsersHelpers;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('role:administrator');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('modules.users.index');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        $user = User::query();

        return DataTables::eloquent($user)
            ->addColumn('actions', 'modules.users.partials.actions')
            ->addColumn('image', 'modules.users.partials.image')
            ->addColumn('role', 'modules.users.partials.roles')
            ->rawColumns(['actions', 'image', 'status', 'role'])
            ->removeColumn('deleted_at')
            ->removeColumn('updated_at')
            ->toJson();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all()->pluck('display_name', 'name');
        $paises = Pais::all()->pluck('nombre', 'id');

        return view('modules.users.create', [
            'roles' => $roles,
            'paises' => $paises,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->createUser($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $roles = Role::all()->pluck('display_name', 'name');

        return view('modules.users.show', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $tiposvehiculos = TipoVehiculos::all()->pluck('nombre', 'id');


        $roles = Role::all()->pluck('display_name', 'name');

        $paises = Role::all()->pluck('nombre', 'id');

        $role_selected = $user->roles->pluck('name')->toArray();

        $paises_seleccionados = $user->pais->pluck('id')->toArray();

        return view('modules.users.edit', [
            'user' => $user,
            'roles' => $roles,
            'role_selected' => $role_selected,
            'tiposvehiculos' => $tiposvehiculos,
            'paises' => $paises,
            'paises_seleccionados' => $paises_seleccionados
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        return $this->updateUser($user, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    {
        switch ($user) {
            case $user->email == "admin@admin.com":
                return $this->adminDeleteResponse();
                break;
            case $user->roles()->first()->name == "administrator";
                return $this->roleAdminAssigned();
            default:
                return $this->deleteUser($user);
                break;
        }
    }
}
