<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|min:4|unique:roles,id, ' . $this->get('id'),
            'description' => 'required|min:6',
            'permissions' => 'required'
        ];
    }

    public function messages()
    {
        return [
          'name.required' => __('validation.roles.update.name-required'),
          'name.unique' => __('validation.roles.update.name-unique'),
          'name.min'  => __('validation.roles.update.name-min'),
          'description.required' => __('validation.roles.update.description-required'),
          'description.min'   => __('validation.roles.update.description-min'),
          'permissions.required'  => __('validation.roles.update.permission-required')
        ];
    }
}
