<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPago extends Model
{
    const ACTIVO = true;
    const INACTIVO = false;

    protected $table = "tipos_pagos";

    protected $fillable = ['nombre', 'activo'];
}
