<div class="table-responsive">
    <table class="table table-hover table-bordered table-striped" id="users-table">
        <thead>
        <th width="10px">@lang('modules.users.users-table.image')</th>
        <th>@lang('modules.users.users-table.name')</th>
        <th>@lang('modules.users.users-table.role')</th>
        <th width="140">@lang('modules.users.users-table.actions')</th>
        </thead>
        <tbody></tbody>
    </table>
</div>
