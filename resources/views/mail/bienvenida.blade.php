<!DOCTYPE html>
<html>
<head>
	<title>Bienvenido(a). </title>
</head>
<style type="text/css">
	
</style>
<body>
	<div class="principal">
		<h3>Bienvenido a la comunidad Urbanplus</h3>
		<div>
			<img src="<?php echo asset('images/logos/logo_plus.jpg') ?>">
		</div>
		<p>
			Estimada o estimado, <?php echo $user->name . ' ' .$user->surname ?>,
		</p>
		<p>
			Me complace darle la más cordial bienvenida a la gran famila Urbanplus.
		</p>

		
		<?php 
			if($user->tipo_usuario == 2){
		?>
			<p>
				Sus datos están siendo analisados para su respectiva aprobación.
			</p>
			<p>
				En su momento recibirá un correo notificando su aprobación.
			</p>
		<?php 
			}
		?>
	</div>
</body>
</html>