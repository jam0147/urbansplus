@can('users.show')
    <a href="{{ route('users.show', $id) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="@lang('modules.users.users-show')">
        <i class="icon-eye"></i>
    </a>
@endcan

@can('users.edit')
    <a href="{{ route('users.edit', $id) }}" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="@lang('modules.users.users-edit')">
        <i class="icon-edit"></i>
    </a>
@endcan

@can('users.destroy')
    <button type="button" class="btn btn-danger" onclick="destroy({{ $id }})" data-toggle="tooltip" data-placement="top" title="@lang('modules.users.users-destroy')">
        <i class="icon-trash"></i>
    </button>
@endcan
