<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = "paises";

    protected $fillable = ['nombre', 'code'];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
