<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">

            <li class="navigation-header">
                <span>@lang('sidebar.main-navigation')</span>
                <i data-toggle="tooltip" data-placement="right" data-original-title="@lang('sidebar.main-navigation')" class="icon-ellipsis icon-ellipsis"></i>
            </li>

            <li class="nav-item {{ active_route('dashboard') }}">
                <a href="{{ route('dashboard') }}">
                    <i class="icon-dashboard"></i>
                    <span class="menu-title">
                        @lang('sidebar.dashboard')
                    </span>
                </a>
            </li>

            <li class="nav-item {{ active_route('profile') }}">
                <a href="{{ route('profile', Auth::user()->id) }}">
                    <i class="icon-head"></i>
                    <span class="menu-title">
                        @lang('navbar.my-profile')
                    </span>
                </a>
            </li>

            <li class="navigation-header">
                <span>@lang('sidebar.modules')</span>
                <i data-toggle="tooltip" data-placement="right" data-original-title="@lang('sidebar.modules')" class="icon-ellipsis icon-ellipsis"></i>
            </li>

            <li class=" nav-item">
                @can('users.index', 'users.create', 'users.edit', 'users.show', 'roles.index', 'roles.create',
                'roles.edit', 'roles.show', 'permissions.index')
                <a href="#">
                    <i class="icon-ios-people"></i>
                    <span class="menu-title">@lang('sidebar.users-management')</span>
                </a>
                <ul class="menu-content">

                    @can('users.index', 'users.create', 'users.edit', 'users.show')
                    <li class="{{ active_route('users.*') }}">
                        <a href="{{ route('users.index') }}" class="menu-item">
                            <i class="icon-minus-round"></i>
                            @lang('sidebar.users')
                        </a>
                    </li>
                    @endcan

                    @can('roles.index', 'roles.create', 'roles.edit', 'roles.show')
                    <li class="{{ active_route('roles.*') }}">
                        <a href="{{ route('roles.index') }}" class="menu-item">
                            <i class="icon-minus-round"></i>
                            @lang('sidebar.roles')
                        </a>
                    </li>
                    @endcan

                    @can('permissions.index')
                    <li class="{{ active_route('permissions.*') }}">
                        <a href="{{ route('permissions.index') }}" class="menu-item">
                            <i class="icon-minus-round"></i>
                            @lang('sidebar.permissions')
                        </a>
                    </li>
                    @endcan

                </ul>
                @endcan
            </li>

            <li class="nav-item {{ active_route('filemanager') }}">
                <a href="{{ route('filemanager') }}">
                    <i class="icon-folder-upload"></i>
                    <span class="menu-title">
                        @lang('Administrador de archivos')
                    </span>
                </a>
            </li>

        </ul>
    </div>
</div>
