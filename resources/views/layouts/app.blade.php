<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" data-textdirection="ltr" class="loading">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} - @yield('page_title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    @stack('styles')
</head>

<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns fixed-navbar">
    @include('partials.main-header')
    @include('partials.main-sidebar')
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
                @include('partials.page-header')
            </div>
            <div class="content-body">
                @yield('content')
            </div>
        </div>
    </div>

    @include('partials.main-footer')
    @routes
    <!--<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>-->
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('select').select2({
                theme: 'bootstrap4',
            });
        });
        window.default_locale = "{{ config('app.locale') }}";
        window.fallback_locale = "{{ config('app.fallback_locale') }}";
        window.localizations = @json($localizations);
    </script>
    <!--<script src="{{ asset('js/scripts.js') }}"></script>-->
    <script src="{{ asset('js/fileupload/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('js/fileupload/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('js/fileupload/jquery.fileupload.js') }}"></script>
    @stack('scripts')
</body>

</html>
