<?php

return [
    'info' => 'Mostrando de _START_ a _END_ de _TOTAL_ entradas',
    'emptyTable' => 'No tiene datos disponibles.',
    'infoEmpty' => 'Mostrando de 0 a 0 de 0 entradas',
    'infoFiltered' => '(filtrado de _MAX_ entradas totales)',
    'lengthMenu' => 'Mostrar _MENU_ entradas',
    'loadingRecords' => 'Cargando...',
    'processing' => 'Procesando...',
    'search' => 'Buscar:',
    'zeroRecords' => 'No se encontraron registros coincidentes',
    'paginate' => [
        'first' => 'Primero',
        'last'  => 'Último',
        'next' => 'Siguiente',
        'previous' => 'Anterior'
    ],
    'aria' => [
        'sortAscending' => ': activar para ordenar la columna ascendente',
        'sortDescending' => ': activar para ordenar la columna descendente'
    ]
];
