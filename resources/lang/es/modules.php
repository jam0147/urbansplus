<?php

return [
    'users' => [
        'users-header' => 'Usuarios',
        'users-title'   => 'Listado de usuarios registrados en el sistema',
        'users-create' => 'Crear nuevo usuario',
        'users-show' => 'Detalles del usuario',
        'users-edit' => 'Editar usuario',
        'users-destroy' => 'Eliminar usuario',
        'users-table' => [
            'image' => 'Imagen',
            'name' => 'Nombre',
            'role' => 'Rol',
            'actions' => 'Acciones'
        ],
        'users-form' => [
            'edit-user' => 'Editar usuario',
            'create-user' => 'Crear usuario',
            'users-form-creation' => 'Formulario de creación de usuarios',
            'users-form-edit' => 'Formulario de edición de usuarios',
            'name' => 'Nombre:',
            'email' => 'Correo electrónico:',
            'assign-role' => 'Asignar rol:',
            'enter-name' => 'Ingresa nombre',
            'enter-email' => 'Ingresa correo electrónico',
            'select-role' => 'Seleccione un rol',
            'go-back' => 'Volver',
            'save' => 'Guardar',
            'save-changes' => 'Guardar cambios',
            'role' => 'Rol',
        ],
        'no-role-assigned' => 'Sin rol asignado',
        'users-messages' => [
            'are-you-sure' => '¿Estas seguro?',
            'text-confirm-delete' => 'Confirma que desea eliminar',
            'text-confirm-update' => 'Confirma que desea guardar cambios',
            'cancel' => 'No, cancelar',
            'confirm-delete' => 'Si, Eliminalo',
            'confirm-update' => 'Si, guarda los cambios',
            'not-delete-admin' => 'El administrador del sistema no puede ser eliminado',
            'canceled' => 'Cancelado',
            'no-deleted' => 'No se eliminó ningún registro',
            'successful-operation' => 'Operación exitosa',
            'user-removed' => 'El usuario a sido eliminado con éxito',
            'user-created' => 'El usuario a sido creado con éxito',
            'user-updated' => 'El usuario a sido actualizado con éxito',
            'operation-not-allowed' => 'Operación no permitida',
            'no-delete-user-with-role-admin' => 'No se puede eliminar un usuario con el rol de administrador asignado.',
            'no-save-changes' => 'Los cambios no fueron guardados'
        ]
    ],

    'roles' => [
        'roles-header' => 'Roles',
        'roles-title'   => 'Listado de roles del sistema',
        'roles-create' => 'Crear nuevo rol',
        'roles-show' => 'Detalles del rol',
        'roles-edit' => 'Editar rol',
        'roles-destroy' => 'Eliminar rol',
        'roles-table' => [
            'role' => 'Rol',
            'description' => 'Descripción',
            'actions' => 'Acciones',
            'created' => 'Creado'
        ],
        'roles-form' => [
            'edit-role' => 'Editar rol',
            'create-role' => 'Crear rol',
            'roles-form-creation' => 'Formulario de creación de roles',
            'roles-form-edit' => 'Formulario de edición de roles',
            'name' => 'Nombre:',
            'description' => 'Descripción:',
            'assign-permissions' => 'Asignar permisos:',
            'enter-name' => 'Ingresa nombre del rol',
            'enter-description' => 'Ingresa breve descripción del rol',
            'select-permissions' => 'Seleccione permisos',
            'go-back' => 'Volver',
            'save' => 'Guardar',
            'save-changes' => 'Guardar cambios',
        ],
        'roles-messages' => [
            'are-you-sure' => '¿Estas seguro?',
            'text-confirm-delete' => 'Confirma que desea eliminar',
            'text-confirm-update' => 'Confirma que desea guardar cambios',
            'cancel' => 'No, cancelar',
            'confirm-delete' => 'Si, Eliminalo',
            'confirm-update' => 'Si, guarda los cambios',
            'not-delete-role-default' => 'No se pueden eliminar los roles por defectos del sistema',
            'canceled' => 'Cancelado',
            'no-deleted' => 'No se eliminó ningún registro',
            'successful-operation' => 'Operación exitosa',
            'role-removed' => 'El rol a sido eliminado con éxito',
            'role-created' => 'El rol a sido creado con éxito',
            'role-updated' => 'El rol a sido actualizado con éxito',
            'operation-not-allowed' => 'Operación no permitida',
            'no-save-changes' => 'Los cambios no fueron guardados'
        ]
    ],

    'permissions' => [
        'permissions-header' => 'Permisos',
        'permissions-title'   => 'Listado de permisos del sistema',
        'permissions-table' => [
            'permission' => 'Permiso',
            'description' => 'Descripción',
            'actions' => 'Acciones',
            'created' => 'Creado'
        ],
    ],

    'profile' => [
        'profile-header' => 'Mi perfil',
        'profile-title' => 'Detalles de mi cuenta',
        'form' => [
            'name' => 'Nombre',
            'email' => 'Correo electrónico',
            'password' => 'Contraseña',
            'change-password' => 'Cambiar contraseña',
            'save-changes' => 'Guardar cambios',
            'role' => 'Rol',
            'go-back' => 'Volver',
        ],
        'messages' => [
            'change-password' => 'Contraseña cambiada con éxito'
        ]
    ]
];
