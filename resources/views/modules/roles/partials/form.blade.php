@if (isset($role))
{!! Form::model($role, ['class' => 'form', 'id' => $id]) !!}
@else
{!! Form::open(['class' => 'form', 'id' => $id]) !!}
@endif

<div class="row">
    <div class="col-lg-6 offset-lg-3">
        <div class="form-group">
            {!! Form::label(__('modules.roles.roles-form.name')) !!}
            {!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder' => __('modules.roles.roles-form.enter-name'),
            'id' =>
            'name', 'autofocus']) !!}
            <div class="text-danger display-hidden" id="error_name"></div>
        </div>

        <div class="form-group">
            {!! Form::label(__('modules.roles.roles-form.description')) !!}
            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => __('modules.roles.roles-form.enter-description'),
            'id' =>
            'description']) !!}
            <div class="text-danger display-hidden" id="error_description"></div>
        </div>

        <div class="form-group">
            {!! Form::label(__('modules.roles.roles-form.assign-permissions')) !!}
            {!! Form::select('permissions[]', $permissions, isset($permissions_selected) ? $permissions_selected : null, ['class' => 'form-control', 'id' => 'permissions', 'multiple']) !!}
            <div class="text-danger display-hidden" id="error_permissions"></div>
        </div>

    </div>
</div>
<div class="form-actions center">
    <a href="{{ route('roles.index') }}" class="btn btn-warning mr-1">
        <i class="icon-arrow-left4"></i>
        @lang('modules.roles.roles-form.go-back')
    </a>
    <button type="submit" class="btn btn-primary" id="submit-button">
        <i class="icon-check2"></i>
        {{ $btn_text }}
    </button>
</div>
{!! Form::close() !!}
