<?php

return [
    'dashboard' => 'Dashboard',
    'modules' => 'Modules',
    'users-management' => 'Users management',
    'users' => 'Users',
    'permissions' => 'Permissions',
    'roles' => 'Roles',
    'main-navigation' => 'Main navigation'
];
