<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehiculos extends Model
{
    protected $table = "vehiculos";
    protected $fillable = [
        'user_id', 'vehiculos_tipos_id', 'matricula', 'foto', 'aprobado'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Set the user's passwords.
     *
     * @param  string  $password
     * @return void
     */

    
}
