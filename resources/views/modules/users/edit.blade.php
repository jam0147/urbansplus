@extends('layouts.app')

@section('page_title', __("modules.users.users-edit") ." ". $user->name)

@section('breadcrumbs')
    {{ Breadcrumbs::render('users.edit', $user) }}
@endsection

@section('content')
    @include('modules.users.partials.form', [
        'paises' => $paises, 'roles' => $roles, 'method' => 'PUT',
        'btn_text' => 'Actualizar usuario'
    ])
@endsection

@push('scripts')
    <script type="text/javascript">

        $("#user_form_update").on('submit', function (event) {
            event.preventDefault();
            $("#submit-button").attr("disabled", true);
            $("#name").removeClass('border-red');
            $("#email").removeClass('border-red');
            $("#role").removeClass('border-red');
            $("#error_name").addClass('display-hidden').text("");
            $("#error_email").addClass('display-hidden').text("");
            $("#error_role").addClass('display-hidden').text("");

            let form_data = {
                name: $("#name").val(),
                email: $("#email").val(),
                surname: $("#surname").val(),
                role: $("#role").val(),
                direccion: $("#direccion").val(),
                aprobado: $("#aprobado").val(),
                confirmado: $("#confirmado").val(),
                tlf: $("#tlf").val(),
                pais: $("#pais").val()
            };

            swal({
                title: "@lang('modules.users.users-messages.are-you-sure')",
                text: "@lang('modules.users.users-messages.text-confirm-update')",
                icon: 'warning',
                buttons: {
                    cancel: {
                        text: "@lang('modules.users.users-messages.cancel')",
                        value: false,
                        visible: true,
                        className: "btn btn-danger mr-1",
                        closeModal: true
                    },
                    confirm: {
                        text: "@lang('modules.users.users-messages.confirm-update')",
                        value: true,
                        visible: true,
                        className: "btn btn-success",
                        closeModal: true
                    }
                },
            }).then((result) => {
                if (result === true) {
                    $.ajax({
                        type: "PUT",
                        url: "{{ route('users.update', $user->id) }}",
                        data: {
                            name: form_data.name,
                            email: form_data.email,
                            role: form_data.role,
                            surname: form_data.surname,
                            direccion: form_data.direccion,
                            aprobado: form_data.aprobado,
                            tlf: form_data.tlf,
                            pais: form_data.pais
                        },
                        dataType: "JSON",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }).success(function (response) {
                        $("#submit-button").attr("disabled", false);
                        console.log(response);
                        swal({
                            icon: 'success',
                            title: response.title,
                            text: response.message
                        });
                    }).error(function (response) {
                        $("#submit-button").attr("disabled", false);
                        console.log(response);
                        swal({
                            icon: 'error',
                            title: response.responseJSON.message,
                            text: response.statusText
                        });

                        let error_name = response.responseJSON.errors.name;
                        let error_email = response.responseJSON.errors.email;
                        let error_role = response.responseJSON.errors.role;

                        if (error_name) {
                            console.error(error_name.toString());
                            $("#name").addClass('border-red');
                            $("#error_name").removeClass('display-hidden').text(error_name.toString());
                        }

                        if (error_email) {
                            console.error(error_email.toString());
                            $("#email").addClass('border-red');
                            $("#error_email").removeClass('display-hidden').text(error_email.toString());
                        }

                        if (error_role) {
                            console.error(error_role.toString());
                            $("#role").addClass('border-red');
                            $("#error_role").removeClass('display-hidden').text(error_role.toString());
                        }
                    });
                } else {
                    $("#submit-button").attr("disabled", false);
                    swal({
                        title: "@lang('modules.users.users-messages.canceled')",
                        text: "@lang('modules.users.users-messages.no-save-changes')",
                        icon: "error"
                    });
                };
            });
        });
        $('#file2').fileupload({
          url: "{{ route('users.licencia') }}",
          dataType: 'json',
          start: function(){
                $(".loaderFile", "#licencia").fadeIn();

          },
          done: function (e, data) {
            $("#resultado2").html(data.result.msj);
            $(".loaderFile", "#licencia").fadeOut();
          }
      });
    </script>
@endpush
