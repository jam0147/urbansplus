<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>{{ config('app.name') }} - {{ __('Under maintenance') }}</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/ico/favicon-32.png') }}">
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column bg-maintenance-image blank-page blank-page">

    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body"><section class="flexbox-container">
                    <div class="col-md-6 offset-md-3 col-xs-12 p-0">
                        <div class="card border-grey border-lighten-3 px-1 py-1 box-shadow-3">
                            <div class="card-block">
                                <span class="card-title text-xs-center">
                                    <img src="{{ asset('images/logo/robust-logo-dark-big.png') }}" class="img-fluid mx-auto d-block pt-2" width="250" alt="logo">
                                </span>
                            </div>
                            <div class="card-block text-xs-center">
                                <h3>{{ __('This system is under maintenance') }}</h3>
                                <p>{{ __("We're sorry for the inconvenience.") }}
                                    <br> {{ __('Please check back later.') }}</p>
                                <div class="mt-2"><i class="icon-gear-a spinner font-large-2"></i></div>
                            </div>
                            <hr>
                            <p class="socialIcon card-text text-xs-center pt-2 pb-2">
                                <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-facebook">
                                    <span class="icon-facebook3"></span>
                                </a>
                                <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-twitter">
                                    <span class="icon-twitter3"></span>
                                </a>
                                <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-linkedin">
                                    <span class="icon-linkedin3 font-medium-4"></span>
                                </a>
                                <a href="#" class="btn btn-social-icon mr-1 mb-1 btn-outline-github">
                                    <span class="icon-github font-medium-4"></span>
                                </a>
                            </p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/scripts') }}" type="text/javascript"></script>
</body>
</html>
