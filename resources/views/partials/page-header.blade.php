<div class="content-header-left col-md-6 col-xs-12 mb-1">
    <h2 class="content-header-title">@yield('page_title')</h2>
</div>
<div class="content-header-right breadcrumbs-right breadcrumbs-top col-md-6 col-xs-12">
    <div class="breadcrumb-wrapper col-xs-12">
        @yield('breadcrumbs')
    </div>
</div>