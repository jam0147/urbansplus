@extends('layouts.app')

@section('page_title', __('modules.users.users-header'))

@section('breadcrumbs')
    {{ Breadcrumbs::render('users.index') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title float-lg-left text-uppercase">
                        @lang('modules.users.users-title')
                    </h4>
                    @can('users.create')
                        <a href="{{ route('users.create') }}" class="btn btn-primary float-lg-right text-uppercase">
                            @lang('modules.users.users-create')
                        </a>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="card-block">
                        @include('modules.users.partials.table')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">

        let dataTable;

        // Read users...
        $(function(){

            dataTable = $("#users-table").DataTable({
                processing: true,
                ajax: {
                    url: "{{ route('users.data') }}",
                    type: "GET"
                },
                columns: [
                    { data: "image", orderable: false, searchable: false },
                    { data: "name" },
                    { data: "role" },
                    { data: "actions", orderable: false, searchable: false },
                ],
                language: {
                    "emptyTable":     "@lang('datatables.emptyTable')",
                    "info":           "@lang('datatables.info')",
                    "infoEmpty":      "@lang('datatables.infoEmpty')",
                    "infoFiltered":   "@lang('datatables.infoFiltered')",
                    "infoPostFix":    "",
                    "infoThousands":  ",",
                    "lengthMenu":     "@lang('datatables.lengthMenu')",
                    "loadingRecords": "@lang('datatables.loadingRecords')",
                    "processing":     "@lang('datatables.processing')",
                    "search":         "@lang('datatables.search')",
                    "zeroRecords":    "@lang('datatables.zeroRecords')",
                    "paginate": {
                        "first":    "@lang('datatables.paginate.first')",
                        "last":     "@lang('datatables.paginate.last')",
                        "next":     "@lang('datatables.paginate.next')",
                        "previous": "@lang('datatables.paginate.previous')"
                    },
                    "aria": {
                        "sortAscending":  "@lang('datatables.aria.sortAscending')",
                        "sortDescending": "@lang('datatables.aria.sortDescending')"
                    }
                }
            });

        });

        // Delete users
        function destroy(id) {

            swal({
                title: "@lang('modules.users.users-messages.are-you-sure')",
                text: "@lang('modules.users.users-messages.text-confirm-delete')",
                icon: 'warning',
                buttons: {
                    cancel: {
                        text: "@lang('modules.users.users-messages.cancel')",
                        value: false,
                        visible: true,
                        className: "btn btn-danger mr-1",
                        closeModal: true
                    },
                    confirm: {
                        text: "@lang('modules.users.users-messages.confirm-delete')",
                        value: true,
                        visible: true,
                        className: "btn btn-success",
                        closeModal: true
                    }
                },
            }).then((result) => {

                if (result == true) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "DELETE",
                        url: "/admin/users/" + id,
                        dataType: "JSON",
                    }).success(function (response) {

                        dataTable.ajax.reload();
                        swal({
                            icon: 'success',
                            title: response.title,
                            text: response.message
                        });
                    }).error(function (response) {
                        console.log(response);
                        swal({
                            icon: 'error',
                            title: response.responseJSON.title,
                            text: response.responseJSON.message
                        });
                    });
                } else if (result === false) {
                    swal({
                        title: "@lang('modules.users.users-messages.canceled')",
                        text: "@lang('modules.users.users-messages.no-deleted')",
                        icon: "error"
                    });
                }
            });
        }
    </script>
@endpush
