<?php

Route::view('/', 'home.index')->name('home');

/*
    |--------------------------------------------------------------------------
    | Change language
    |--------------------------------------------------------------------------
    */
Route::get('lang/{lang}', 'ChangeLanguageController@changeLanguage')
    ->name('lang');
Route::get('firebase',  'Fire\FirebaseController@index');

Route::prefix('admin')->group(function (){
    /*
 |--------------------------------------------------------------------------
 | Auth Routes
 |--------------------------------------------------------------------------
 */
    Route::auth([
        'verify' => false,
        'register' => false
    ]);

    /*
    |--------------------------------------------------------------------------
    | Lock and Unlock Account
    |--------------------------------------------------------------------------
    */
    Route::get('lock-account', 'Auth\LockAccountController@lockAccount')
        ->name('lock')
        ->middleware('auth');

    Route::post('unlock', 'Auth\LockAccountController@unLockAccount')
        ->name('unlock')
        ->middleware('auth');

    /*
    |--------------------------------------------------------------------------
    | App Routes
    |--------------------------------------------------------------------------
    */
    Route::middleware(['auth', 'verified', 'unlock'])->group(function () {

        Route::get('/', 'DashboardController@index')
            ->name('dashboard');

        // Users routes...

        Route::resource('users', 'Modules\UserController');

        Route::get('users-data', 'Modules\UserController@getData')
            ->name('users.data');

        // Permissions routes...

        Route::get('permissions', 'Modules\PermissionController@index')
            ->name('permissions.index');

        Route::get('permissions/data', 'Modules\PermissionController@getData')
            ->name('permissions.data');

        // Roles routes...

        Route::resource('roles', 'Modules\RoleController');

        Route::get('roles-data', 'Modules\RoleController@getData')
            ->name('roles.data');

        Route::get('roles/data/permissions/{id}', 'Modules\RoleController@permisionsRoles')
            ->name('roles.permissions.data');

        // Profile routes...
        Route::get('profile/{id}', 'ProfileController@index')
            ->name('profile');

        Route::put('profile/{id}/change-password', 'ProfileController@changePassword')
            ->name('profile.change-password');



        Route::get('users-licencia', 'Modules\UserController@licencia')
            ->name('users.licencia');

        Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
            \UniSharp\LaravelFilemanager\Lfm::routes();
        });

        Route::view('filemanager', 'filemanager.index')->name('filemanager');

        Route::get('pasajeros-datatable', 'DashboardController@dataTablePasajerosRegistrados')
            ->name('pasajeros-datatable');

        Route::get('conductores-datatable', 'DashboardController@dataTableConductoresRegistrados')
            ->name('conductores-datatable');

        Route::get('conductores-pendientes-datatable', 'DashboardController@dataTableConductoresPendientes')
            ->name('conductores-pendientes-datatable');
    });
});
