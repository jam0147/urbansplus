@extends('layouts.app')

@section('page_title', __('modules.roles.roles-edit')." ". $role->name)

@section('breadcrumbs')
    {{ Breadcrumbs::render('roles.edit', $role) }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title float-lg-left text-uppercase">
                        @lang('modules.roles.roles-form.roles-form-edit')
                    </h4>
                </div>
                <div class="card-body">
                    <div class="card-block">
                        @include('modules.roles.partials.form', ['id' => 'edit-role-form', "role" => $role, 'permissions' => $permissions, 'permissions_selected' => $permissions_selected, 'btn_text' => __('modules.roles.roles-form.save-changes')])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

<script type="text/javascript">

    $("#edit-role-form").on('submit', function (event) {
        event.preventDefault();

        $("#submit-button").attr("disabled", true);
        $("#name").removeClass('border-red');
        $("#description").removeClass('border-red');
        $("#permissions").removeClass('border-red');
        $("#error_name").addClass('display-hidden').text("");
        $("#error_description").addClass('display-hidden').text("");
        $("#error_permissions").addClass('display-hidden').text("");

        let formData = {
            name: $("#name").val(),
            description: $("#description").val(),
            permissions: $("#permissions").val()
        };

        swal({
            title: "@lang('modules.roles.roles-messages.are-you-sure')",
            text: "@lang('modules.roles.roles-messages.text-confirm-update')",
            icon: 'warning',
            buttons: {
                cancel: {
                    text: "@lang('modules.roles.roles-messages.cancel')",
                    value: false,
                    visible: true,
                    className: "btn btn-danger mr-1",
                    closeModal: true
                },
                confirm: {
                    text: "@lang('modules.roles.roles-messages.confirm-update')",
                    value: true,
                    visible: true,
                    className: "btn btn-success",
                    closeModal: true
                }
            },
        }).then((result) => {

            if(result === true) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{ route('roles.update', $role->id) }}",
                    type: "PUT",
                    dataType: "JSON",
                    data: {
                        name: formData.name,
                        description: formData.description,
                        permissions: formData.permissions
                    }
                }).success(function (response) {
                    $("#submit-button").attr("disabled", false);
                    console.log(response);
                    swal({
                        icon: 'success',
                        title: response.title,
                        text: response.message
                    });
                }).error(function (response) {
                    $("#submit-button").attr("disabled", false);
                    console.error(response);
                    swal({
                        icon: 'error',
                        title: response.responseJSON.message,
                        text: response.statusText
                    });

                    let error_name = response.responseJSON.errors.name;
                    let error_description = response.responseJSON.errors.description;
                    let error_permissions = response.responseJSON.errors.permissions;

                    if (error_name) {
                        console.error(error_name.toString());
                        $("#name").addClass('border-red');
                        $("#error_name").removeClass('display-hidden').text(error_name.toString());
                    }

                    if (error_description) {
                        console.error(error_description.toString());
                        $("#description").addClass('border-red');
                        $("#error_description").removeClass('display-hidden').text(error_description.toString());
                    }

                    if (error_permissions) {
                        console.error(error_permissions.toString());
                        $("#permissions").addClass('border-red');
                        $("#error_permissions").removeClass('display-hidden').text(error_permissions.toString());
                    }

                });
            } else {
                $("#submit-button").attr("disabled", false);
                swal({
                    title: "@lang('modules.roles.roles-messages.canceled')",
                    text: "@lang('modules.roles.roles-messages.no-save-changes')",
                    icon: "error"
                });
            }
        });
    });

</script>
@endpush
