<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|min:6',
            'email'         => 'required|min:6|email|unique:users',
            'role'          => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'       => __("validation.users.store.name-required"),
            'name.min'            => __("validation.users.store.name-min"),
            'name.alpha'          => __("validation.users.store.name-alphanumeric"),
            'email.required'      => __("validation.users.store.email-required"),
            'email.min'           => __("validation.users.store.email-min"),
            'email.unique'        => __("validation.users.store.email-unique"),
            'email.email'         => __('validation.users.store.email-email'),
            'role.required'       => __('validation.users.store.rol-required')
        ];
    }
}
