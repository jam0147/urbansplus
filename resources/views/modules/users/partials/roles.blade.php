@if(!$model->getRoleNames()->first() == "")
    <span class="tag tag-primary">
        {{ $model->getRoleNames()->first() }}
    </span>
    @else
    <span class="tag tag-default">
        @lang('modules.users.no-role-assigned')
    </span>
@endif
