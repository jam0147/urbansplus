<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@signup');
Route::post('restaurar', 'Api\AuthController@restaurar');
Route::post('detectar_pais', 'Api\AuthController@detectarRegion');
Route::get('obtener-paises', 'Api\AuthController@obtenerPaises');


Route::group(['middleware' => 'auth:api'], function(){

	Route::get('logout', 'Api\AuthController@logout');
    Route::get('user', 'Api\AuthController@user');

    Route::post('solicitud_viaje', 'Api\AuthController@Solicitudviajes');
    Route::post('solicitud_respuesta', 'Api\AuthController@Solicitudrespuesta');
	Route::post('drive', 'Api\AuthController@drive');
	Route::post('perfil_update', 'Api\AuthController@UpdatePerfil');
	Route::post('clave_update', 'Api\AuthController@updateClave');
	Route::post('perfil', 'Api\AuthController@perfil');
	Route::post('viajes_activos', 'Api\AuthController@viajesActivos');
	Route::post('historial_viajes', 'Api\AuthController@historial_viajes');

	Route::post('subir_imagen', 'Api\AuthController@subir_imagen');
	Route::post('cancelar_viaje', 'Api\AuthController@cancelar_viaje');

	Route::post('enviar_Mensaje', 'Api\AuthController@enviar_Mensaje');

});



