@extends('layouts.auth')

@section('page_title', __('auth.reset-password'))

@section('content')
    <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
        <div class="card-header no-border pb-0">
            <div class="card-title text-xs-center">
                <img src="{{ asset('images/logo/logo-dark.png') }}" alt="branding logo">
            </div>
            <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2">
                <span>@lang('auth.send-email-description')</span>
            </h6>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        </div>
        <div class="card-body collapse in">
            <div class="card-block">
                <form class="form-horizontal" action="{{ route('password.email') }}" method="POST" novalidate>

                    @csrf

                    <fieldset class="form-group position-relative has-icon-left">
                        <input type="email" name="email" value="{{ old('email') }}" class="form-control form-control-lg input-lg {{ $errors->has('email') ? 'border-danger' : '' }}" id="user-email" placeholder="@lang('auth.enter-email')" required autofocus>
                        <div class="form-control-position">
                            <i class="icon-mail6"></i>
                        </div>
                        @if ($errors->has('email'))
                            <span class="text-danger">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </fieldset>

                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                        <i class="icon-envelope mr-1"></i> @lang('auth.send-link')
                    </button>

                </form>
            </div>
        </div>
    </div>
@endsection
