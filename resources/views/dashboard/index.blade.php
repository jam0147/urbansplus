@extends('layouts.app')

@section('page_title', __('sidebar.dashboard'))

@section('breadcrumbs')
    {{ Breadcrumbs::render('dashboard') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-3 col-lg-3 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="icon-map1 cyan font-large-2 float-xs-left"></i>
                            </div>
                            <div class="media-body text-xs-right">
                                <h3>{{ $total_viajes }}</h3>
                                <span>Viajes</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="icon-user1 cyan font-large-2 float-xs-left"></i>
                            </div>
                            <div class="media-body text-xs-right">
                                <h3>{{ $total_pasajeros }}</h3>
                                <span>Pasajeros</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="icon-users cyan font-large-2 float-xs-left"></i>
                            </div>
                            <div class="media-body text-xs-right">
                                <h3>{{ $total_conductores }}</h3>
                                <span>Conductores</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="icon-users cyan font-large-2 float-xs-left"></i>
                            </div>
                            <div class="media-body text-xs-right">
                                <h3>{{ $total_conductores_pendientes }}</h3>
                                <span>Cond. por aprobar</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Estadisticas</h4>
                </div>
                <div class="card-block">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true">
                                Viajes realizados
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="base-tab2" data-toggle="tab" aria-controls="tab2" href="#tab2" aria-expanded="false">
                                Pasajeros registrados
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="base-tab3" data-toggle="tab" aria-controls="tab3" href="#tab3" aria-expanded="false">
                                Conductores registrados
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="base-tab4" data-toggle="tab" aria-controls="tab4" href="#tab4" aria-expanded="false">
                                Conductores por aprobar
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" style="margin-top: 10px">
                        <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">

                        </div>
                        <div class="tab-pane" id="tab2" aria-labelledby="base-tab2">
                            <table class="table table-bordered table-hover table-striped" id="pasajeros-table">
                                <thead>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab3" aria-labelledby="base-tab3">
                            <table class="table table-bordered table-hover table-striped" id="conductores-table">
                                <thead>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Correo</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab4" aria-labelledby="base-tab4">
                            <table class="table table-bordered table-hover table-striped" id="conductores-pendientes-table">
                                <thead>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Correo</th>
                                <th width="40px">Acción</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>
        table.dataTable {
            clear: both;
            margin-top: 6px !important;
            margin-bottom: 6px !important;
            max-width: none !important;
            border-collapse: separate !important;
            border-spacing: 0;
            width: 100%!important;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $("#pasajeros-table").DataTable({
            processing: true,
            ajax: {
                url: "{{ route('pasajeros-datatable') }}",
                type: "GET"
            },
            columns: [
                { data: "name" },
                { data: "surname" },
            ],
            language: {
                "emptyTable":     "@lang('datatables.emptyTable')",
                "info":           "@lang('datatables.info')",
                "infoEmpty":      "@lang('datatables.infoEmpty')",
                "infoFiltered":   "@lang('datatables.infoFiltered')",
                "infoPostFix":    "",
                "infoThousands":  ",",
                "lengthMenu":     "@lang('datatables.lengthMenu')",
                "loadingRecords": "@lang('datatables.loadingRecords')",
                "processing":     "@lang('datatables.processing')",
                "search":         "@lang('datatables.search')",
                "zeroRecords":    "@lang('datatables.zeroRecords')",
                "paginate": {
                    "first":    "@lang('datatables.paginate.first')",
                    "last":     "@lang('datatables.paginate.last')",
                    "next":     "@lang('datatables.paginate.next')",
                    "previous": "@lang('datatables.paginate.previous')"
                },
                "aria": {
                    "sortAscending":  "@lang('datatables.aria.sortAscending')",
                    "sortDescending": "@lang('datatables.aria.sortDescending')"
                }
            }
        });

        $("#conductores-table").DataTable({
            processing: true,
            ajax: {
                url: "{{ route('conductores-datatable') }}",
                type: "GET"
            },
            columns: [
                { data: "name" },
                { data: "surname" },
                { data: "email" },
            ],
            language: {
                "emptyTable":     "@lang('datatables.emptyTable')",
                "info":           "@lang('datatables.info')",
                "infoEmpty":      "@lang('datatables.infoEmpty')",
                "infoFiltered":   "@lang('datatables.infoFiltered')",
                "infoPostFix":    "",
                "infoThousands":  ",",
                "lengthMenu":     "@lang('datatables.lengthMenu')",
                "loadingRecords": "@lang('datatables.loadingRecords')",
                "processing":     "@lang('datatables.processing')",
                "search":         "@lang('datatables.search')",
                "zeroRecords":    "@lang('datatables.zeroRecords')",
                "paginate": {
                    "first":    "@lang('datatables.paginate.first')",
                    "last":     "@lang('datatables.paginate.last')",
                    "next":     "@lang('datatables.paginate.next')",
                    "previous": "@lang('datatables.paginate.previous')"
                },
                "aria": {
                    "sortAscending":  "@lang('datatables.aria.sortAscending')",
                    "sortDescending": "@lang('datatables.aria.sortDescending')"
                }
            }
        });

        $("#conductores-pendientes-table").DataTable({
            processing: true,
            ajax: {
                url: "{{ route('conductores-pendientes-datatable') }}",
                type: "GET"
            },
            columns: [
                { data: "custom_name" },
                { data: "surname" },
                { data: "email" },
                { data: "button" },
            ],
            language: {
                "emptyTable":     "@lang('datatables.emptyTable')",
                "info":           "@lang('datatables.info')",
                "infoEmpty":      "@lang('datatables.infoEmpty')",
                "infoFiltered":   "@lang('datatables.infoFiltered')",
                "infoPostFix":    "",
                "infoThousands":  ",",
                "lengthMenu":     "@lang('datatables.lengthMenu')",
                "loadingRecords": "@lang('datatables.loadingRecords')",
                "processing":     "@lang('datatables.processing')",
                "search":         "@lang('datatables.search')",
                "zeroRecords":    "@lang('datatables.zeroRecords')",
                "paginate": {
                    "first":    "@lang('datatables.paginate.first')",
                    "last":     "@lang('datatables.paginate.last')",
                    "next":     "@lang('datatables.paginate.next')",
                    "previous": "@lang('datatables.paginate.previous')"
                },
                "aria": {
                    "sortAscending":  "@lang('datatables.aria.sortAscending')",
                    "sortDescending": "@lang('datatables.aria.sortDescending')"
                }
            }
        });
    </script>
@endpush
