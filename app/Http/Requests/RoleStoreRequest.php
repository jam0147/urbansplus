<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|min:4|unique:roles',
            'description' => 'required|min:6',
            'permissions' => 'required'
        ];
    }

    /**
     * messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('validation.roles.store.name-required'),
            'name.unique' => __('validation.roles.store.name-unique'),
            'name.min'  => __('validation.roles.store.name-min'),
            'description.required' => __('validation.roles.store.description-required'),
            'description.min'   => __('validation.roles.store.description-min'),
            'permissions.required'  => __('validation.roles.store.permission-required')
        ];
    }
}
