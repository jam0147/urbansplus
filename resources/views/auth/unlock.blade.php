<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>{{ config('app.name') }} - @lang('auth.unlock-account')</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/ico/favicon-32.png') }}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">

    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-2 p-0">
                        <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                            <div class="card-header no-border text-xs-center">
                                <img src="{{asset('images/avatars')}}/{{ auth()->user()->avatar }}" alt="unlock-user" class="rounded-circle img-fluid center-block">
                                <h5 class="card-title mt-1">{{ $user->name }}</h5>
                            </div>

                            <p class="card-subtitle line-on-side text-muted text-xs-center font-small-3 mx-2">
                                <span>@lang('auth.unlock-account')</span>
                            </p>
                            <div class="card-body collapse in">
                                <div class="card-block">
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif

                                    <form class="form-horizontal" action="{{ route('unlock') }}" method="POST" novalidate>
                                        @csrf
                                        <fieldset class="form-group position-relative has-icon-left" name="password">
                                            <input type="password" class="form-control form-control-lg input-lg" name="password" placeholder="@lang('auth.enter-password')" required>
                                            <div class="form-control-position">
                                                <i class="icon-key3"></i>
                                            </div>
                                        </fieldset>
                                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                                            <i class="icon-unlock2 mr-1"></i> @lang('auth.unlock')
                                        </button>
                                        <a class="btn btn-danger btn-lg btn-block text-white" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                            <i class="icon-power3 mr-1"></i> @lang('auth.logout')
                                        </a>
                                    </form>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    @routes
    <script>
        window.default_locale = "{{ config('app.locale') }}";
        window.fallback_locale = "{{ config('app.fallback_locale') }}";
        window.localizations = @json($localizations);
    </script>
    <script src="{{ asset('js/scripts.js') }}" type="text/javascript"></script>
</body>
</html>
