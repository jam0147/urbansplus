<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\TipoPago::class, function (Faker $faker) {

    $nombres = ['Cheque', 'transferencia electronica', 'tarjeta de credito', 'efectivo'];
    $activo = [App\Models\TipoPago::INACTIVO, App\Models\TipoPago::ACTIVO];

    return [
        'nombre' => $faker->randomElement($nombres),
        'activo' => $faker->randomElement($activo),
    ];
});
