<?php

namespace App\Helpers;

trait Helpers
{
    /**
     * @param $title
     * @param $message
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function returnJson($title, $message, $status)
    {
        return response()->json([
            'title' => $title,
            'message'   => $message
        ], $status);
    }
}
