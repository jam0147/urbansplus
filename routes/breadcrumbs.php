<?php


// Home
Breadcrumbs::for('dashboard', function ($trail) {
    $trail->push(__('breadcrumbs.dashboard'), route('dashboard'));
});

// Users index...
Breadcrumbs::for('users.index', function ($trail) {
   $trail->parent('dashboard');
   $trail->push(__('breadcrumbs.users.index'), route('users.index'));
});

// Users create...
Breadcrumbs::for('users.create', function ($trail) {
   $trail->parent('users.index');
   $trail->push(__('breadcrumbs.users.create'), route('users.create'));
});

// User show...
Breadcrumbs::for('users.show', function ($trail, $user) {
   $trail->parent('users.index');
   $trail->push(__('breadcrumbs.users.details'), route('users.show', $user->id));
});

// User edit...
Breadcrumbs::for('users.edit', function ($trail, $user) {
   $trail->parent('users.index');
   $trail->push(__('breadcrumbs.users.edit'), route('users.edit', $user->id));
});

// Permissions index...
Breadcrumbs::for('permissions.index', function ($trail) {
   $trail->parent('dashboard');
   $trail->push(__('breadcrumbs.permissions'), route('permissions.index'));
});

// Roles index...
Breadcrumbs::for('roles.index', function ($trail) {
   $trail->parent('dashboard');
   $trail->push(__('breadcrumbs.roles.index'), route('roles.index'));
});

// Roles create...
Breadcrumbs::for('roles.create', function ($trail) {
   $trail->parent('roles.index');
   $trail->push(__('breadcrumbs.roles.create'), route('roles.create'));
});

// Roles edit
Breadcrumbs::for('roles.edit', function ($trail, $role) {
    $trail->parent('roles.index');
    $trail->push(__('breadcrumbs.roles.edit'), route('roles.edit', $role->id));
});

// Roles show
Breadcrumbs::for('roles.show', function ($trail, $role) {
    $trail->parent('roles.index');
    $trail->push(__('breadcrumbs.roles.details') , route('roles.edit', $role->id));
});

// Profile
Breadcrumbs::for('profile', function ($trail, $user) {
    $trail->parent('dashboard');
    $trail->push(__('modules.profile.profile-header') , route('profile', $user->id));
});

Breadcrumbs::for('filemanager', function ($trail) {
    $trail->parent('dashboard');
    $trail->push(__('Administrador de archivos'), route('filemanager'));
});
