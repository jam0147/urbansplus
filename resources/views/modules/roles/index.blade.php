@extends('layouts.app')

@section('page_title', __('modules.roles.roles-header'))

@section('breadcrumbs')
    {{ Breadcrumbs::render('roles.index') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title float-lg-left text-uppercase">
                        @lang('modules.roles.roles-title')
                    </h4>
                    @can('roles.create')
                        <a href="{{ route('roles.create') }}" class="btn btn-primary float-lg-right text-uppercase">
                            @lang('modules.roles.roles-create')
                        </a>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="card-block">
                        @include('modules.roles.partials.table')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">

        let dataTable;

        $(function () {
            dataTable = $("#roles-table").DataTable({
                processing: true,
                ajax: {
                    url: "{{ route('roles.data') }}",
                    type: "GET"
                },
                columns: [
                    { data: "id"},
                    { data: "display_name" },
                    { data: "description" },
                    { data: "created" },
                    { data: "actions", orderable: false, searchable: false },
                ],
                language: {
                    "emptyTable":     "@lang('datatables.emptyTable')",
                    "info":           "@lang('datatables.info')",
                    "infoEmpty":      "@lang('datatables.infoEmpty')",
                    "infoFiltered":   "@lang('datatables.infoFiltered')",
                    "infoPostFix":    "",
                    "infoThousands":  ",",
                    "lengthMenu":     "@lang('datatables.lengthMenu')",
                    "loadingRecords": "@lang('datatables.loadingRecords')",
                    "processing":     "@lang('datatables.processing')",
                    "search":         "@lang('datatables.search')",
                    "zeroRecords":    "@lang('datatables.zeroRecords')",
                    "paginate": {
                        "first":    "@lang('datatables.paginate.first')",
                        "last":     "@lang('datatables.paginate.last')",
                        "next":     "@lang('datatables.paginate.next')",
                        "previous": "@lang('datatables.paginate.previous')"
                    },
                    "aria": {
                        "sortAscending":  "@lang('datatables.aria.sortAscending')",
                        "sortDescending": "@lang('datatables.aria.sortDescending')"
                    }
                }
            });
        });

        function destroy(id) {
            swal({
              title: "@lang('modules.roles.roles-messages.are-you-sure')",
              text: "@lang('modules.roles.roles-messages.text-confirm-delete')",
              icon: 'warning',
              buttons: {
                  cancel: {
                      text: "@lang('modules.roles.roles-messages.cancel')",
                      value: false,
                      visible: true,
                      className: "btn btn-danger mr-1",
                      closeModal: true
                  },
                  confirm: {
                      text: "@lang('modules.roles.roles-messages.confirm-delete')",
                      value: true,
                      visible: true,
                      className: "btn btn-success",
                      closeModal: true
                  }
                },
            }).then((result) => {
                if (result === true) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "DELETE",
                        url: "roles/" + id,
                        dataType: "JSON",
                    }).success(function (response) {

                        dataTable.ajax.reload();

                        swal({
                            icon: 'success',
                            title: response.title,
                            text: response.message
                        });
                    }).error(function (response) {
                        console.log(response);
                        swal({
                            icon: 'error',
                            title: response.responseJSON.title,
                            text: response.responseJSON.message
                        });
                    });
                } else if (result === false) {
                  swal({
                      title: "@lang('modules.roles.roles-messages.canceled')",
                      text: "@lang('modules.roles.roles-messages.no-deleted')",
                      icon: "error"
                  });
                }
            });
        }
    </script>
@endpush
