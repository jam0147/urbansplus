<?php

namespace App\Helpers;

use Spatie\Permission\Models\Role;
use App\Helpers\Helpers;

trait RolesHelpers {

    use Helpers;

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRole($request)
    {
        $role = Role::create([
            'name' => $this->roleName($request),
            'display_name' => $request->input('name'),
            'description'   => $request->input('description')
        ]);
        $role->givePermissionTo($request->input('permissions'));
        $title = __('modules.roles.roles-messages.successful-operation');
        $message = __('modules.roles.roles-messages.role-created');
        $status = 200;

        return $this->returnJson($title, $message, $status);
    }

    /**
     * @param $request
     * @return string
     */
    public function roleName($request)
    {
        $role_name_string = str_replace(" ", "", $request->input('name'));
        $role_name = $role_name_string;
        return strtolower($role_name);
    }

    /**
     * @param $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteRole($role)
    {
        $role->delete();
        $title = __('modules.roles.roles-messages.successful-operation');
        $message = __('modules.roles.roles-messages.role-removed');
        $status = 200;

        return $this->returnJson($title, $message, $status);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function defaultRolesResponse()
    {
        $title = __('modules.roles.roles-messages.operation-not-allowed');
        $message = __('modules.roles.roles-messages.not-delete-role-default');
        $status = 400;

        return $this->returnJson($title, $message, $status);
    }

    /**
     * @param $request
     * @param $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateRole($request, $role)
    {
        $role->update([
            'name' => $this->roleName($request),
            'display_name' => $request->input('name'),
            'description'   => $request->input('description')
        ]);
        $role->givePermissionTo($request->input('permissions'));
        $title = __('modules.roles.roles-messages.successful-operation');
        $message = __('modules.roles.roles-messages.role-updated');
        $status = 200;

        return $this->returnJson($title, $message, $status);
    }

}
