<div class="table-responsive">
    <table class="table table-bordered table-hover table-striped" id="roles-table">
        <thead>
            <th width="10px">ID</th>
            <th>@lang('modules.roles.roles-table.role')</th>
            <th>@lang('modules.roles.roles-table.description')</th>
            <th width="90px">@lang('modules.roles.roles-table.created')</th>
            <th width="160px">@lang('modules.roles.roles-table.actions')</th>
        </thead>
        <tbody></tbody>
    </table>
</div>
