let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.sass('resources/scss/style.scss', 'public/css/style.css');

 mix.scripts([
    'resources/js/core/libraries/jquery.min.js',
    'resources/vendors/js/ui/tether.min.js',
    'resources/js/core/libraries/bootstrap.min.js',
    'resources/vendors/js/ui/perfect-scrollbar.jquery.min.js',
    'resources/vendors/js/ui/unison.min.js',
    'resources/vendors/js/ui/blockUI.min.js',
    'resources/vendors/js/ui/jquery.matchHeight-min.js',
    'resources/vendors/js/ui/screenfull.min.js',
    'resources/vendors/js/extensions/pace.min.js',
    'resources/vendors/js/charts/chart.min.js',
    'resources/js/core/app-menu.js',
    'resources/js/core/app.js',
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
    'node_modules/sweetalert/dist/sweetalert.min.js',
    'node_modules/select2/dist/js/select2.full.js',
    'node_modules/axios/dist/axios.js',
    'node_modules/vue/dist/vue.js',
    'node_modules/pusher-js/dist/web/pusher.js',
    'node_modules/laravel-echo/dist/echo.js',
    'resources/vendors/js/dropzone/dropzone.js'
 ], 'public/js/scripts.js');

 mix.copy('resources/images', 'public/images');
