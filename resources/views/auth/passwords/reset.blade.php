@extends('layouts.auth')

@section('page_title', __('auth.reset-password'))

@section('content')
    <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
        <div class="card-header no-border">
            <div class="card-title text-xs-center">
                <img src="{{ asset('images/logo/logo-dark.png') }}" alt="branding logo">
            </div>
            <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2">
                <span>@lang('auth.reset-password')</span>
            </h6>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        </div>
        <div class="card-body collapse in">
            <div class="card-block">
                <form class="form-horizontal form-simple" action="{{ route('password.update') }}" method="POST" novalidate>

                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <fieldset class="form-group position-relative has-icon-left mb-1">
                        <input type="email" name="email" value="{{ old('email', $email) }}" class="form-control form-control-lg input-lg" id="user-email" placeholder="@lang('auth.enter-email')" required>
                        <div class="form-control-position">
                            <i class="icon-mail6"></i>
                        </div>
                        @if ($errors->has('email'))
                            <span class="text-danger">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </fieldset>

                    <fieldset class="form-group position-relative has-icon-left mb-1">
                        <input type="password" name="password" class="form-control form-control-lg input-lg" id="user-password" placeholder="@lang('auth.enter-password')" required>
                        <div class="form-control-position">
                            <i class="icon-key3"></i>
                        </div>
                        @if ($errors->has('password'))
                            <span class="text-danger">
                                {{ $errors->first('password') }}
                            </span>
                        @endif
                    </fieldset>

                    <fieldset class="form-group position-relative has-icon-left">
                        <input type="password" name="password_confirmation" class="form-control form-control-lg input-lg" id="user-password" placeholder="@lang('auth.confirm-password')" required>
                        <div class="form-control-position">
                            <i class="icon-key3"></i>
                        </div>
                        @if ($errors->has('password_confirmation'))
                            <span class="text-danger">
                                {{ $errors->first('password_confirmation') }}
                            </span>
                        @endif
                    </fieldset>

                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                        <i class="icon-unlock2 mr-1"></i> @lang('auth.reset-password')
                    </button>

                </form>
            </div>
        </div>
    </div>
@endsection
