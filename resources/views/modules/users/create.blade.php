@extends('layouts.app')

@section('page_title', __('modules.users.users-form.create-user'))

@section('breadcrumbs')
    {{ Breadcrumbs::render('users.create') }}
@endsection

@section('content')
    @if (session('message'))
        <div class="alert alert-success no-border mb-2" role="alert">
            <strong>{{ session('message') }}</strong>
        </div>
    @endif
    @include('modules.users.partials.form', [
        'paises' => $paises,
        'roles' => $roles,
        'method' => 'POST',
        'btn_text' => 'Registrar usuario',
        'route' => 'users.store'
    ])
@endsection

@push('scripts')
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script>
        $("#my-dropzone").dropzone();
    </script>
@endpush
