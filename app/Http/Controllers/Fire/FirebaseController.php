<?php

namespace App\Http\Controllers\Fire;

use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class FirebaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Conexion()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://prueba-71c30.firebaseio.com/')
        ->create();

        return $firebase->getDatabase();
        
    }
    public function create($datos, $table, $id, $idR){
        $db = $this->Conexion();

        try {
            $NewUser = $db
            ->getReference($table . '/' . $id . '/' . $idR)
            ->set($datos);
        } catch (Exception $e) {
            return false;
        }
           
        return $NewUser->getKey();
    }

    public function createChat($datos, $table, $idR){
        $db = $this->Conexion();

        try {
            $NewUser = $db
            ->getReference($table . '/' . $idR)
            ->push($datos);
        } catch (Exception $e) {
            return false;
        }
           
        return $NewUser->getKey();
    }

    public function update($key, $viajes_id, $datos, $table){
        $db = $this->Conexion();
        
        $updates = [
            $table . '/'.$key . '/' . $viajes_id => $datos
        ];

        try {
            $db->getReference() // this is the root reference
                ->update($updates);
        } catch (Exception $e) {
            return false;
        }
        return true;

        
    }
    public function delete($key, $viajes_id, $table){
        $db = $this->Conexion();
        
        try {

            $db->getReference($table . '/'.$key . '/' . $viajes_id)
                ->remove();

        } catch (Exception $e) {
            return false;
        }
        return true;
    }

}
