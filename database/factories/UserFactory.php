<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'surname' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'direccion' => $faker->address,
        'password' => 'secret',
        'aprobado' => $faker->randomElement([App\Models\User::APROBADO, App\Models\User::RECHAZADO]),
        'remember_token' => str_random(10),
        'dni' => $faker->numberBetween(20000, 3000),
        'confirmado' => $faker->randomElement([App\Models\User::CONFORMADO, App\Models\User::NOCONFIRMADO]),
        'pais_id' => App\Models\Pais::all()->random()->id,
        'tlf' => $faker->phoneNumber
    ];
});
