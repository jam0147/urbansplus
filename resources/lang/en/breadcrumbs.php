<?php

return [
    'dashboard' => 'Dashboard',
    'users' => [
        'index' => 'Users',
        'create' => 'Create user',
        'details' => 'Details',
        'edit' => 'Edit',
    ],
    'roles' => [
        'index' => 'Roles',
        'create' => 'Create role',
        'details' => 'details',
        'edit' => 'Edit',
    ],
    'permissions' => 'Permissions',
];
