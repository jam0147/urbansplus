<?php

return [
    'users' => [
        'users-header' => 'Users',
        'users-title'   => 'List Of registered users in the system',
        'users-create' => 'Create new user',
        'users-show' => 'User details',
        'users-edit' => 'Edit user',
        'users-destroy' => 'Delete user',
        'users-table' => [
            'image' => 'Image',
            'name' => 'Name',
            'role' => 'Role',
            'actions' => 'Actions'
        ],
        'users-form' => [
            'create-user' => 'Create user',
            'users-form-creation' => 'User creation form',
            'name' => 'Name:',
            'email' => 'E-mail:',
            'assign-role' => 'Assign role:',
            'enter-name' => 'Enter name',
            'enter-email' => 'Enter email',
            'select-role' => 'Select a role',
            'go-back' => 'Go back',
            'save' => 'Save',
            'save-changes' => 'Save changes',
            'role' => 'Role',
        ],
        'no-role-assigned' => 'No assigned role',
        'users-messages' => [
            'are-you-sure' => 'Are you sure?',
            'text-confirm-delete' => 'Confirm that you want to delete',
            'text-confirm-update' => 'Confirm that you want to save changes',
            'cancel' => 'No, cancel',
            'confirm-delete' => 'Yes, Delete',
            'confirm-update' => 'Yes, save the changes',
            'not-delete-admin' => 'The system administrator can not be deleted',
            'canceled' => 'Cancelled',
            'no-deleted' => 'No record was deleted',
            'successful-operation' => 'Successful operation',
            'user-removed' => 'The user has been successfully eliminated',
            'user-created' => 'The user has been created successfully',
            'user-updated' => 'The user has been updated successfully',
            'operation-not-allowed' => 'Operation not allowed',
            'no-delete-user-with-role-admin' => 'A user with the assigned administrator role can not be deleted.',
            'no-save-changes' => 'The changes were not saved'
        ]
    ],

    'roles' => [
        'roles-header' => 'Roles',
        'roles-title'   => 'List of system roles',
        'roles-create' => 'Create new role',
        'roles-show' => 'Role details',
        'roles-edit' => 'Edit role',
        'roles-destroy' => 'Delete role',
        'roles-table' => [
            'role' => 'Role',
            'description' => 'Description',
            'actions' => 'Actions',
            'created' => 'Created'
        ],
        'roles-form' => [
            'edit-role' => 'Edit role',
            'create-role' => 'Create role',
            'roles-form-creation' => 'Form roles creation',
            'roles-form-edit' => 'Role edit form',
            'name' => 'Name:',
            'description' => 'Description:',
            'assign-permissions' => 'Assign permissions:',
            'enter-name' => 'Enter role name',
            'enter-description' => 'Enter brief description of the role',
            'select-permissions' => 'Select permissions',
            'go-back' => 'Go back',
            'save' => 'Save',
            'save-changes' => 'Save changes',
        ],
        'roles-messages' => [
            'are-you-sure' => 'Are you sure?',
            'text-confirm-delete' => 'Confirm that you want to delete',
            'text-confirm-update' => 'Confirm that you want to save changes',
            'cancel' => 'No, cancel',
            'confirm-delete' => 'Yes, delete',
            'confirm-update' => 'Yes, save the changes',
            'not-delete-role-default' => 'You can not delete the roles due to system defects',
            'canceled' => 'Canceled',
            'no-deleted' => 'No record was deleted',
            'successful-operation' => 'Successful operation',
            'role-removed' => 'The role has been successfully removed',
            'role-created' => 'The role has been created successfully',
            'role-updated' => 'The role has been updated successfully',
            'operation-not-allowed' => 'Operation not allowed',
            'no-save-changes' => 'The changes were not saved'
        ]
    ],

    'permissions' => [
        'permissions-header' => 'Permissions',
        'permissions-title'   => 'List of system permissions',
        'permissions-table' => [
            'permission' => 'Permission',
            'description' => 'Description',
            'actions' => 'Actions',
            'created' => 'Created'
        ],
    ],

    'profile' => [
        'profile-header' => 'My profile',
        'profile-title' => 'Details of my account',
        'form' => [
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
            'change-password' => 'Change password',
            'save-changes' => 'Save Changes',
            'role' => 'Role',
            'go-back' => 'Go back',
        ],
        'messages' => [
            'change-password' => 'Password changed successfully'
        ]
    ]
];
