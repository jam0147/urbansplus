<table class="table table-bordered table-hover table-striped" id="permissions-table">
    <thead>
        <th width="15px">ID</th>
        <th>@lang('modules.permissions.permissions-table.permission')</th>
        <th>@lang('modules.permissions.permissions-table.description')</th>
        <th width="90px">@lang('modules.permissions.permissions-table.created')</th>
    </thead>
    <tbody></tbody>
</table>
