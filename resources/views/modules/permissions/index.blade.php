@extends('layouts.app')

@section('page_title', __('modules.permissions.permissions-header'))

@section('breadcrumbs')
    {{ Breadcrumbs::render('permissions.index') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title text-uppercase">
                        @lang('modules.permissions.permissions-title')
                    </h4>
                </div>
                <div class="card-body">
                    <div class="card-block">
                        @include('modules.permissions.partials.table')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')

@endpush

@push('scripts')
    <script type="text/javascript">
        let table = $(document).ready(function () {
            $("#permissions-table").DataTable({
                processing: true,
                ajax: {
                    url: "{{ route('permissions.data') }}",
                    type: "GET"
                },
                columns: [
                    { data: "id"},
                    { data: "display_name" },
                    { data: "description" },
                    { data: "created" },
                ],
                language: {
                    "emptyTable":     "@lang('datatables.emptyTable')",
                    "info":           "@lang('datatables.info')",
                    "infoEmpty":      "@lang('datatables.infoEmpty')",
                    "infoFiltered":   "@lang('datatables.infoFiltered')",
                    "infoPostFix":    "",
                    "infoThousands":  ",",
                    "lengthMenu":     "@lang('datatables.lengthMenu')",
                    "loadingRecords": "@lang('datatables.loadingRecords')",
                    "processing":     "@lang('datatables.processing')",
                    "search":         "@lang('datatables.search')",
                    "zeroRecords":    "@lang('datatables.zeroRecords')",
                    "paginate": {
                        "first":    "@lang('datatables.paginate.first')",
                        "last":     "@lang('datatables.paginate.last')",
                        "next":     "@lang('datatables.paginate.next')",
                        "previous": "@lang('datatables.paginate.previous')"
                    },
                    "aria": {
                        "sortAscending":  "@lang('datatables.aria.sortAscending')",
                        "sortDescending": "@lang('datatables.aria.sortDescending')"
                    }
                }
            });
        });
    </script>
@endpush
