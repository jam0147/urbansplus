<!DOCTYPE html>
<!--
-->
<html>
<head>
    <title>::: Urbans Plus :::</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="{{ asset('layout/styles/layout.css') }}" rel="stylesheet" type="text/css" media="all">
</head>
<body id="top">
<div class="wrapper row1"><br>
</div>
<div class="wrapper row2">
    <nav id="mainav" class="clear">
        <ul class="clear">
            <li class="active"><a href="{{ route('home') }}">INICIO</a></li>
            <li><a class="drop" href="#">CATEGORIAS</a>
                <ul>
                    <li><a href="http://www.urbansplus.com/contactenos.html">Plus City</a></li>
                    <li><a href="http://www.urbansplus.com/contactenos.html">Plus Sedan</a></li>
                    <li><a href="http://www.urbansplus.com/contactenos.html">Plus Premium</a></li>
                    <li><a href="http://www.urbansplus.com/contactenos.html">Plus Ultra Suv</a></li>
                    <li><a href="http://www.urbansplus.com/contactenos.html">Plus Cargo</a></li>
                </ul>
            </li>
            <li><a class="drop" href="#">USUARIOS</a>
                <ul>
                    <li><a href="#">Pasajeros</a></li>
                    <li><a class="drop" href="#">Conductores</a>
                        <ul>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="#">QUIENES SOMOS</a></li>
            <li><a href="#">REGISTRARSE</a></li>
            <li><a href="http://www.urbansplus.com/contactenos.html" target="new">CONTACTENOS</a></li>
            <br>
            <i class=""><img src="http://www.urbansplus.com/chile.png" alt="">...</i>
        </ul>
    </nav>
</div>
<div class="wrapper row3">
    <div id="slider" class="clear">
        <div class="flexslider basicslider">
            <ul class="slides">
                <li><a href="#"><img class="radius-10" src="images/demo/slides/01.png" alt=""></a></li>
                <li><a href="#"><img class="radius-10" src="images/demo/slides/02.png" alt=""></a></li>
                <li><a href="#"><img class="radius-10" src="images/demo/slides/03.png" alt=""></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="wrapper row3">
    <main class="container clear">
        <ul class="nospace group btmspace-80">
            <li class="one_third first">
                <article class="service">
                    <h6 class="heading"><a href="#">URBANS Plus</a></h6>
                    <p>Somos una empresa de Transporte de Pasajeros, operada en Chile  con  capitales chilenos. Existimos para resolver con eficiencia y calidad las problematicas del transporte privado.</p>
                    <footer><a href="http://www.urbansplus.com/contactenos.html">Leer más &raquo;</a></footer>
                </article>
            </li>
            <li class="one_third">
                <article class="service">
                    <h6 class="heading"><a href="#">URBANS Plus</a></h6>
                    <p>Nuestro centro de operaciones está en la ciudad de Viña del Mar; Región de Valparaiso  desde donde controlamos las operaciones hacia el resto del país. Contamos con una flota de vehículos de excelencia.</p>
                    <footer><a href="http://www.urbansplus.com/contactenos.html">Leer más &raquo;</a></footer>
                </article>
            </li>
            <li class="one_third">
                <article class="service">
                    <h6 class="heading"><a href="#">URBANS Plus</a></h6>
                    <p>Nuestros asociados: Empresarios, Pasajeros y Conductores privilegian por sobre todo, la eficiencia, calidad y claridad en  todas las áreas de nuestros  servicios.</p>
                    <footer><a href="http://www.urbansplus.com/contactenos.html">Leer más&raquo;</a></footer>
                </article>
            </li>
        </ul>
        <h2 class="sectiontitle">URBANS Plus</h2>
        <div class="clear"></div>
    </main>
</div>
<div class="wrapper row4">
    <footer id="footer" class="clear">
        <div class="one_quarter first">
            <h6 class="title">Nuestra empresa </h6>
            <address class="btmspace-15">
                URBANS PLUS<br>
                Viña del Mar<br>
                Región de Valparaiso<br>
                CHILE
            </address>
            <ul class="nospace">
                <li class=""></li>
                <li><span class="fa fa-envelope-o"></span> contacto@urbansplus.com</li>
            </ul>
        </div>
        <div class="one_quarter">
            <h6 class="title">Accesos </h6>
            <ul class="nospace linklist">
                <li><a href="http://www.urbansplus.com/contactenos.html">Nosotros</a></li>
                <li><a href="http://www.urbansplus.com/contactenos.html">F.A.Q.</a></li>
                <li><a href="http://www.urbansplus.com/contactenos.html">Documentos</a></li>
                <li><a href="http://www.urbansplus.com/contactenos.html">Terminos y Condiciones</a></li>
                <li><a href="http://www.urbansplus.com/contactenos.html">Contáctenos</a></li>
            </ul>
        </div>
        <div class="one_quarter">
            <h6 class="title">Hora actual</h6>
            <td width="266"><div style="text-align:center;padding:1em 0;"> <h4>Viña del Mar, Chile</h4><iframe src="https://www.zeitverschiebung.net/clock-widget-iframe-v2?language=es&size=small&timezone=America%2FSantiago" width="100%" height="90" frameborder="0" seamless></iframe>
                </div></td></p>
            </article>
        </div>
        <div class="one_quarter">
            <h6 class="title">Síguenos y Escríbenos</h6>
            <form class="btmspace-30" method="post" action="#">
                <fieldset>
                    <legend></legend>
                </fieldset>
                <blockquote>
                    <fieldset type="submit" value="submit">
                        <a href="http://www.urbansplus.com/contactenos.html" target="new">
                            <button type="submit" value="submit">Contacto </button>
                        </a>
                    </fieldset>
                </blockquote>
            </form>
            <ul class="faico clear">
                <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a class="faicon-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                <li><a class="faicon-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a class="faicon-instagram" href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a class="faicon-tumblr" href="#"><i class="fa fa-tumblr"></i></a></li>
            </ul>
        </div>
    </footer>
</div>
<div class="wrapper row5">
    <div id="copyright" class="clear">
        <p class="fl_left">Copyright &copy; 2018 - Derechos reservados - <a href="#">URBANS Plus</a></p>
        <p class="fl_right">&nbsp;</p>
    </div>
</div>
<span style="font-size: 9px"><a href="http://www.pcmarketing.cl" style="font-size: 9px; color: #A0A0A4;">PC MARKETING </a></span><a id="backtotop" href="#top"></a>
<script src="{{ asset('layout/scripts/jquery.min.js') }}"></script>
<script src="{{ asset('layout/scripts/jquery.backtotop.js') }}"></script>
<script src="{{ asset('layout/scripts/jquery.mobilemenu.js') }}"></script>
<script src="{{ asset('layout/scripts/jquery.flexslider-min.js') }}"></script>
</body>
</html>
