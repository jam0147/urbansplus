<?php

namespace App\Http\Controllers;

class ChangeLanguageController extends Controller
{
    /**
     * @param $lang
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeLanguage($lang)
    {
        session(['lang' => $lang]);
        return redirect()->back();
    }
}
