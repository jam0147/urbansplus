<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $admin = User::create([
            'name'  => 'Administrator',
            'email' => 'admin@admin.com',
            'password'  => 'admin@admin.com'
        ]);

        $admin->assignRole('administrator');
    }
}
