<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoVehiculos extends Model
{
    protected $table = "vehiculos_tipos";
    protected $fillable = [
        'nombre', 'descripcion', 'img'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Set the user's passwords.
     *
     * @param  string  $password
     * @return void
     */

    
}
