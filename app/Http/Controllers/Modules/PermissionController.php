<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use DataTables;

class PermissionController extends Controller
{
    /**
     * PermissionController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:permissions.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('modules.permissions.index');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        $permission = Permission::query();

        return DataTables::eloquent($permission)
            ->addColumn('created', 'modules.permissions.partials.created_at')
            ->rawColumns(['created'])
            ->toJson();
    }
}
