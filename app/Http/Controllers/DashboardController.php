<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Viajes;
use Spatie\Permission\Models\Role;
use DataTables;

class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $total_viajes = Viajes::all()->count();

        $roles_usuarios = \DB::table('model_has_roles')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->join('users', 'model_has_roles.model_id', '=', 'users.id')
            ->get();

        $total_pasajeros = $roles_usuarios->where('display_name', '=', 'Pasajero')->count();
        $total_conductores = $roles_usuarios->where('display_name', '=', 'Conductor')->count();
        $total_conductores_pendientes = $roles_usuarios->where('display_name', '=', 'Conductor')->where('confirmado', '=', null)->count();

        return view('dashboard.index', compact('total_viajes','total_pasajeros', 'total_conductores', 'total_conductores_pendientes'));
    }

    public function dataTablePasajerosRegistrados()
    {

        $roles_usuarios = \DB::table('model_has_roles')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->join('users', 'model_has_roles.model_id', '=', 'users.id')
            ->get();

        $pasajeros_registrados = $roles_usuarios->where('display_name', '=', 'Pasajero');

        return datatables()->of($pasajeros_registrados)->toJson();
    }

    public function dataTableConductoresRegistrados()
    {
        $roles_usuarios = \DB::table('model_has_roles')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->join('users', 'model_has_roles.model_id', '=', 'users.id')
            ->get();

        $conductores_registrados = $roles_usuarios->where('display_name', '=', 'Conductor');

        return datatables()->of($conductores_registrados)->toJson();
    }

    public function dataTableConductoresPendientes()
    {
        $roles_usuarios = \DB::table('model_has_roles')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->join('users', 'model_has_roles.model_id', '=', 'users.id')
            ->get();

        $conductores_pendientes = $roles_usuarios->where('display_name', '=', 'Conductor')->where('confirmado', '=', null);

        return datatables()->of($conductores_pendientes)
            ->addColumn('custom_name', 'dashboard.partials.name')
            ->addColumn('button', 'dashboard.partials.button')
            ->removeColumn('name')
            ->rawColumns(['custom_name', 'button'])
            ->toJson();
    }
}
