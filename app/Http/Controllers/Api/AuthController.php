<?php
namespace App\Http\Controllers\Api;

use App\Mail\RestorePassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Fire\FirebaseController;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Vehiculos;
use App\Models\Documentos;
use App\Models\TipoDocumento;
use App\Models\Viajes;
use App\Models\Pais;
use App\Models\Chat;


use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Storage;
use Validator;
use Mail;

class AuthController extends Controller
{
    public function obtenerPaises()
    {
        //dd(bcrypt('87654321'));
        $paises = Pais::select('nombre', 'id')->orderBy('nombre')->get();

        return response()->json([
            'paises' => $paises
        ]);
    }

    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        /*$request->validate([
            'name' => 'required|string',
            'surname' => 'required|string',
            'dni' => 'required|string',
            'direccion' => 'required|string',
            'tlf' => 'required|string',
            'pais_id' => 'required',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmation',
        ]);*/

        $user = new User([
            'name'          => $request->name,
            'surname'       => $request->surname,
            'tlf'           => $request->tlf,
            'pais_id'       => $request->pais_id,
            'email'         => $request->email,
            'password'      => $request->password
        ]);

        $user->save();

        $role = Role::where('name', 'pasajero')->first();

        $user->assignRole($role);

        Mail::send('mail.bienvenida', ['user' => $user], function ($m) use ($user) {
            $m->from('servidor.email.prueba@gmail.com', 'Urbanplus');
            $m->to($user->email, $user->name)->subject('Nuevo miembro!');
        });

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);

    }

    public function drive(Request $request){
        $token = $this->obtenerToken($request);
        $user = User::where('api_token',  $token)->first();
        $role = Role::where('id', $user->tipo_usuario)->first();

        if ($user) {
            $palabra = '';
            if ($request->tipo_datos == "vehiculo") {

                $validator = Validator::make($request->all(), [
                    'vehiculos_tipos_id'    => 'required',
                    'matricula'             => 'required|string',
                    'licencia'              => 'required|string',
                    'file'                  => 'mimes:jpeg,png,jpg|doc|docx|pdf',
                ]);

                $documento = TipoDocumento::where('descripcion', 'like', '%Vehiculo%')->first();

                if (!$documento) {
                    return response()->json([
                        'message' => 'No existe documento relacionado!'
                    ], 201);
                }

                $files = $request->file('file');

                if($files)
                    $ok = $this->guardarArchivos($files, $user->id, $documento->id);


                if ($ok) {
                    if (!$find = Vehiculos::where('user_id', $user->id)->first()) {
                        $vehiculo = new Vehiculos([
                            'user_id' => $user->id,
                            'vehiculos_tipos_id' => $request->vehiculos_tipos_id,
                            'matricula' => $request->matricula,
                            'foto' => ''
                        ]);
                        $vehiculo->save();

                    }else{
                        $vehiculo = Vehiculos::where('user_id', $user->id)->update([
                            'user_id' => $user->id,
                            'vehiculos_tipos_id' => $request->vehiculos_tipos_id,
                            'matricula' => $request->matricula,
                            'foto' => ''
                        ]);
                    }
                }
                return response()->json([
                    'message' => 'Registro Exitoso!'
                ], 201);
            }
            if ($request->tipo_datos == "licencia")
                $palabra = "%licencia%";

            if ($request->tipo_datos == "hoja_vida")
                $palabra = "%vida%";

            if ($request->tipo_datos == "antecedentes")
                $palabra = '%antecedentes%';

            $documento = TipoDocumento::where('descripcion', 'like', $palabra)->first();

            if (!$documento) {
                return response()->json([
                    'message' => 'No existe documento relacionado!'
                ], 201);
            }

            $files = $request->file('file');

            if($files)
                $ok = $this->guardarArchivos($files, $user->id, $documento->id);

            return response()->json([
                'message' => 'Registro Exitoso!'
            ], 201);
        }
    }

    public function guardarArchivos($files, $usuario_id, $tipo_documento_id)
    {
        $deleteDocument = Documentos::where('tipo_documento_id', $tipo_documento_id)
                                            ->where('user_id', $usuario_id)
                                            ->delete();

        foreach($files as $file) {

           $path = public_path() . '/images/users/' . $usuario_id ;

           $filename = bcrypt(uniqid()) . $file->getClientOriginalName();
           $upload_success = $file->move($path, $filename);

            if ($upload_success) {

               $documentos = new Documentos([
                    'user_id'          => $usuario_id,
                    'tipo_documento_id' => $tipo_documento_id,
                    'ruta'              => $filename
                ]);

                $documentos->save();
            }
        }

        return true;
    }
    public function UpdatePerfil(Request $request)
    {

        $request->validate([
            'name' => 'required|string',
            'surname' => 'required|string'
            
        ]);
        $token = $this->obtenerToken($request);
        $user = User::where('api_token',  $token)->first();
        $foto = 'default-avatar.png';
        if (!$user) {
           return response()->json([
                    's' => 'n',
                    'msj' => 'Usuario no encontrado!!'
                ], 200);
        }

        /*if (isset($request->file('foto'))) {
            $request->validate([
                'foto'      => 'mimes:jpeg,png,jpg',
            ]);
            $file = $request->file('foto');
            $foto = $user->foto;
            if ($file != $user->foto) {
                $path = public_path() . '/images/users/' . $user->id ;
                $filename = bcrypt(uniqid()) . $file->getClientOriginalName();
                $upload_success = $file->move($path, $filename);
                $foto =  $filename;
            }
        }*/



        $userUpdated = User::where('id', $user->id)
                    ->update([
                        'name'          => $request->name,
                        'surname'       => $request->surname,
                        'direccion'     => $request->direccion,
                        'tlf'           => $request->tlf,
                        'dni'           => $request->dni,
                        'email'         => $request->email,
                        'avatar'        => $foto
                    ]);
        if ($userUpdated) {
            return response()->json([
                        'message' => 'Datos actualizados!'
                    ], 201);

        }
        return response()->json([
                        'message' => 'Error!'
                    ], 500);
    }
    public function updateClave(Request $request)
    {
        $request->validate([
            'old_password' => 'required|string',
            'password' => 'required|string',
            'c_password' => 'required|same:password'
        ]);



        $token = $this->obtenerToken($request);
        $user = User::where('api_token',  $token)->first();

        if (!Hash::check($request->old_password, $user->password)) {
            return response()->json([
                        'message' => 'Error la contraseña actual no coincide!'
                    ], 401);
        }
        //dd( $user);

        $userUpdated = User::where('id', $user->id)
                    ->update([
                        'password'          => bcrypt($request->password)
                    ]);

        if ($userUpdated) {
            return response()->json([
                        'message' => 'Password actualizados!'
                    ], 201);

        }

        return response()->json([
                        'message' => 'Error!'
                    ], 500);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function restaurar(Request $request){
        $request->validate([
            'email' => 'required|string|email'
        ]);
        $email = $request->email;
        $user = User::where('email', $email)->first();
        if ($user) {
            $clave = substr(md5(uniqid()), 0, 10);
            User::where('id', $user->id)
            ->update([
                'password' => bcrypt($clave)
            ]);

            $FromEmail = 'lealcristian@gmail.com';
            $Subject = 'Admin Urbeplus';
            $correo = $user->email;
            
            /*
            Mail::send('mail.restore',['usuario' => $user, 'clave' => $clave ], function ($message) use($FromEmail, $correo, $Subject){
                    $message->from($FromEmail, 'Admin Urbeplus');
                    $message->to($correo)->subject($Subject);
            });*/


            Mail::to($correo)->send(new RestorePassword($user, $clave));
            return response()->json([
                'message' => 'Successfully!'
            ], 201);
        }

        return response()->json([
                'message' => 'Unauthorized'
            ], 401);

    }
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if(Auth::attempt($credentials)) {

        } else {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        $userLogin = User::where('email', $request->email)
                        ->update(['api_token' => $tokenResult->accessToken]);

        $user = User::select('users.*', 'paises.nombre as pais', 'paises.code' )
                        ->leftJoin('paises', 'paises.id', '=', 'users.pais_id')
                        ->where('email', $request->email)
                        ->first();

        return response()->json([
            'access_token'  => $tokenResult->accessToken,
            'token_type'    => 'Bearer',
            'expires_at'    => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'user' => $user,
        ]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */

    public function perfil(Request $request){

        $token = $this->obtenerToken($request);
        $user = User::where('api_token',  $token)->first();
        
        if (!$user) {
           return response()->json([
                    's' => 'n',
                    'msj' => 'Usuario no encontrado!!'
                ], 405);
        }
        
        $datosUser = User::select(
                                'users.name','users.surname',
                                'users.avatar','users.email',
                                'users.foto','users.dni',
                                'users.direccion','users.tipo_usuario',
                                'users.tlf','users.pais_id'
                            )
                        ->where('id', $user->id)
                        ->first();

        $vehiculo = Vehiculos::select(
                                'vehiculos.vehiculos_tipos_id','vehiculos.matricula',
                                'vehiculos.aprobado', 'vehiculos_tipos.nombre', 'vehiculos_tipos.descripcion'
                            )
                        ->join('vehiculos_tipos', 'vehiculos_tipos.id', '=', 'vehiculos.vehiculos_tipos_id')
                        ->where('vehiculos.user_id', $datosUser->id)
                        ->get();

        $documentos = Documentos::select(
                                'documentos.tipo_documento_id','documentos.ruta',
                                'tipo_documentos.descripcion'
                            )
                        ->join('tipo_documentos', 'tipo_documentos.id', '=', 'documentos.tipo_documento_id')
                        ->where('documentos.user_id', $datosUser->id)
                        ->get();


        return response()->json([
            'perfil'        => $datosUser,
            'vehiculos'     => $vehiculo,
            'documentos'    => $documentos
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function Solicitudviajes(Request $request){


        $request->validate([
            'inic_coord_inicio'             => 'required',
            'inic_coord_final'              => 'required',
            'tipos_pagos_id'                => 'required'
        ]);
        $token = $this->obtenerToken($request);
        $user = User::where('api_token',  $token)->first();
        if (!$user) {
           return response()->json([
                    's' => 'n',
                    'msj' => 'Usuario no encontrado!!'
                ], 405);
        }

        
       $coordenadasInicio =    $request->inic_coord_inicio['lat']  . ' -- ' . $request->inic_coord_inicio['lng'];

        $coordenadasFinal =    $request->inic_coord_final['lat']  . ' -- ' . $request->inic_coord_final['lng'];
       
        $datos = [
                'pasajero_id'                   => $user->id,
                'solicitud_tipo_vehiculo_id'    => $request->solicitud_tipo_vehiculo_id,
                'tipos_pagos_id'                => $request->tipos_pagos_id,
                'inic_coord_inicio'             => $coordenadasInicio,
                'inic_coord_final'              => $coordenadasFinal,
                'distancia_total_google'        => $request->distanciaTotal,
                'tiempo_total_google'           => $request->tiempoTotal,
                'lugar_destino'                 => $request->lugar_destino,
                'msj_conductor'                 => $request->msj_conductor,
                'costo'                         => 0, 
                'status'                        => 0,
                'fecha_viaje'                   => isset($request->fecha_viaje) ? Carbon::now($request->fecha_viaje)->format('Y-m-d H:i:s') : Carbon::now()->format('Y-m-d H:i:s'),
                'created_at'                    => Carbon::now()->format('Y-m-d H:i:s')
            ];
        
        $datosFire = [
                'pasajero_id'                   => $user->id,
                'conductor_id'                  => 0,
                'vehiculos_id'                  => 0,  
                'solicitud_tipo_vehiculo_id'    => $request->solicitud_tipo_vehiculo_id,
                'tipos_pagos_id'                => $request->tipos_pagos_id,
                'inic_coord_inicio'             => $coordenadasInicio,
                'inic_coord_final'              => $coordenadasFinal,
                'llegada_coord_inicio'          => '0',
                'llegada_coord_final'           => '0', 
                'distancia_total_google'        => $request->distanciaTotal,
                'lugar_destino'                 => $request->lugar_destino,
                'costo'                         => 0,
                'tiempo_total_google'           => $request->tiempoTotal,
                'msj_conductor'                 => $request->msj_conductor,
                'status'                        => 0,
                'calificacion_conductor'        => 0, 
                'calificacion_pasajero'         => 0,
                'fecha_viaje'                   => isset($request->fecha_viaje) && $request->fecha_viaje !== '' ? Carbon::parse($request->fecha_viaje)->format('d/m/Y H:i:s') : Carbon::now()->format('d/m/Y H:i:s'),
                'created_at'                    => Carbon::now()->format('d/m/Y H:i:s')
            ];    

        $solicitud = Viajes::create($datos);

        if ($solicitud) {
            
            $Chat = [
                    'viaje_id'      => $solicitud->id,
                    'user_id'       => $user->id,
                    'mensaje'       => $request->msj_conductor,
                    'created_at'    =>  Carbon::now()
            ];

            $controller = new FirebaseController;
            $key = $controller->create($datosFire, 'viajes', $user->id, $solicitud->id);

            if ($key) {
                $updated = Viajes::where('id', $solicitud->id)->update(['key_firebase'    => $key]);

                if ($request->msj_conductor !== ''){
                    $controller->createChat($Chat, 'chat', $solicitud->id);
                    $chat = Chat::create($Chat);
                }
                

                return response()->json([
                    's'                         => 's',
                    'msj'                       => 'Creada exitosamente!!',
                    'key_firebase_viajes'       => $user->id,
                    'key_firebase_chat'         => $solicitud->id      
                ], 200);
            }
        }

    }
    public function Solicitudrespuesta(Request $request){

        $request->validate([
            'viajes_id'                  => 'required'
        ]);
        $token = $this->obtenerToken($request);
        $user = User::where('api_token',  $token)
                            ->where('tipo_usuario', 2)
                            ->first();

        if (!$user) {
           return response()->json([
                    's' => 'n',
                    'msj' => 'Usuario no encontrado!!'
                ], 405);
        }
                            

        $viaje = Viajes::where('id', $request->viajes_id)
                        ->where('status', 0)
                        ->first();

        if ($viaje && $user) {

            $vehiculo = Vehiculos::where('users_id',  $user->id)
                                    ->where('vehiculos_tipos_id', $viaje->solicitud_tipo_vehiculo_id)
                                    ->first();

            if ($vehiculo) {
                $solicitud = Viajes::where('id', $viaje->id)->update([
                    'conductor_id' => $user->id,
                    'vehiculos_id' => $vehiculo->id,
                    'status' => 1
                ]);

                if ($solicitud) {

                    $datos = [
                        'pasajero_id'                   => $viaje->pasajero_id,
                        'conductor_id'                  => $user->id,
                        'vehiculos_id'                  => $vehiculo->id,  
                        'solicitud_tipo_vehiculo_id'    => $viaje->solicitud_tipo_vehiculo_id,
                        'tipos_pagos_id'                => $viaje->tipos_pagos_id,
                        'inic_coord_inicio'             => $viaje->inic_coord_inicio,
                        'inic_coord_final'              => $viaje->inic_coord_final,
                        'llegada_coord_inicio'          => '0',
                        'llegada_coord_final'           => '0', 
                        'distancia_total_google'        => $viaje->distancia_total_google,
                        'lugar_destino'                 => $viaje->lugar_destino,
                        'costo'                         => $viaje->costo,
                        'tiempo_total_google'           => $viaje->tiempo_total_google,
                        'msj_conductor'                 => $viaje->msj_conductor,
                        'status'                        => 1,
                        'calificacion_conductor'        => 0, 
                        'calificacion_pasajero'         => 0,
                        'fecha_viaje'                   =>  Carbon::parse($viaje->fecha_viaje)->format('d/m/Y H:i:s') ,
                        'created_at'                    => Carbon::parse($viaje->created_at)->format('d/m/Y H:i:s')

                    ];
                    $controller = new FirebaseController;
                    $ok = $controller->update($viaje->pasajero_id, $viaje->id, $datos, 'viajes');


                    return response()->json([
                        's' => 's',
                        'msj' => 'Solicitud Aceptada!!'
                    ]);

                }


            }

            return response()->json([
                    's' => 'n',
                    'msj' => 'Usted no cumple con los requerimientos de la solicitud!!'
                ]);

        }
        return response()->json([
                    's' => 'n',
                    'msj' => 'Ya la solicitud fue aceptada por otra persona !!'
                ]);
    }



    public function viajesActivos(Request $request)
    {
       $token = $this->obtenerToken($request);
       $user = User::where('api_token',  $token)->first();

        $viajes = Viajes::where('pasajero_id',  $user->id)
                        ->whereIn('status',  [0, 1])->get();

        if (!$user) {
           return response()->json([
                    's' => 'n',
                    'msj' => 'Usuario no encontrado!!'
                ], 405);
        }

        $viajes_activos = [];

       foreach ($viajes as $key => $viaje) {
            $descripcion_tipo_pago = '';
            if ($viaje->tipos_pagos_id == 1) {
                $descripcion_tipo_pago = 'Efectivo';
            }elseif ($viaje->tipos_pagos_id == 2) {
                $descripcion_tipo_pago = 'Paypal';
            }else{
                $descripcion_tipo_pago = 'Tarjeta de Crédito';
            }



            $viajes_activos[] = [
                'id'                            => $viaje->id,
                'solicitud_tipo_vehiculo_id'    => $viaje->solicitud_tipo_vehiculo_id,
                'tipos_pagos_id'                => $viaje->tipos_pagos_id,
                'descripcion_tipo_pago'         =>  $descripcion_tipo_pago,
                'solicitud_tipo_vehiculo_id'    => $viaje->solicitud_tipo_vehiculo_id,
                'inic_coord_inicio'             => [
                    'lat' => $this->buscarCoordenadas($viaje->inic_coord_inicio, 'lat'),
                    'lng' => $this->buscarCoordenadas($viaje->inic_coord_inicio, 'lng')
                ],


                'inicio_lat'                    => $this->buscarCoordenadas($viaje->inic_coord_inicio, 'lat'),
                'inicio_lng'                    => $this->buscarCoordenadas($viaje->inic_coord_inicio, 'long'),

                'inic_coord_final'              => [
                    'lat' => $this->buscarCoordenadas($viaje->inic_coord_final, 'lat'),
                    'lng' => $this->buscarCoordenadas($viaje->inic_coord_final, 'lng')
                ],
                'inicio_lat_final'              => $this->buscarCoordenadas($viaje->inic_coord_final, 'lat'),
                'inicio_long_final'             => $this->buscarCoordenadas($viaje->inic_coord_final, 'long'),


                'distancia_total_google'        => $viaje->distancia_total_google,
                'costo'                         => $viaje->costo,
                'key_firebase'                  => $viaje->key_firebase,
                'fecha'                  => Carbon::parse($viaje->created_at)->format('d/m/Y H:i:s'),
                'status'                         => $viaje->status,



            ];
       }



        if ($viajes) {
          return response()->json([
                    's'                 => 's',
                    'msj'               => 'Viajes activos!!',
                    'viajes_activos'    => $viajes_activos
                ], 201);
        }

        return response()->json([
            's'                 => 'n',
            'msj'               => 'No hay viajes activos!!',
            'viajes_activos'    => $viajes_activos
        ], 200);
    }

    public function historial_viajes(Request $request)
    {
        $token = $this->obtenerToken($request);
        $user = User::where('api_token',  $token)->first();

        if (!$user) {
           return response()->json([
                    's' => 'n',
                    'msj' => 'Usuario no encontrado!!'
                ], 500);
        }

        $viajes = Viajes::where('pasajero_id',  $user->id)
                           ->whereNotIn('status', [0, 2])->get();

        $viajes_activos = [];

       foreach ($viajes as $key => $viaje) {
            $descripcion_tipo_pago = '';
            if ($viaje->tipos_pagos_id == 1) {
                $descripcion_tipo_pago = 'Efectivo';
            }elseif ($viaje->tipos_pagos_id == 2) {
                $descripcion_tipo_pago = 'Paypal';
            }else{
                $descripcion_tipo_pago = 'Tarjeta de Crédito';
            }


            $viajes_activos[] = [
                'id'                            => $viaje->id,
                'solicitud_tipo_vehiculo_id'    => $viaje->solicitud_tipo_vehiculo_id,
                'tipos_pagos_id'                => $viaje->tipos_pagos_id,
                'solicitud_tipo_vehiculo_id'    => $viaje->solicitud_tipo_vehiculo_id,
                'descripcion_tipo_pago'         =>  $descripcion_tipo_pago,
                'inic_coord_inicio'             => [
                    'lat' => $this->buscarCoordenadas($viaje->inic_coord_inicio, 'lat'),
                    'lng' => $this->buscarCoordenadas($viaje->inic_coord_inicio, 'long')
                ],


                'inicio_lat'                    => $this->buscarCoordenadas($viaje->inic_coord_inicio, 'lat'),
                'inicio_lng'                    => $this->buscarCoordenadas($viaje->inic_coord_inicio, 'lng'),

                'inic_coord_final'              => [
                    'lat' => $this->buscarCoordenadas($viaje->inic_coord_final, 'lat'),
                    'lng' => $this->buscarCoordenadas($viaje->inic_coord_final, 'long')
                ],
                'inicio_lat_final'              => $this->buscarCoordenadas($viaje->inic_coord_final, 'lat'),
                'inicio_long_final'             => $this->buscarCoordenadas($viaje->inic_coord_final, 'lng'),


                'distancia_total_google'        => $viaje->distancia_total_google,
                'costo'                         => $viaje->costo,
                'key_firebase'                  => $viaje->key_firebase,
                'created_at'                    => Carbon::parse($viaje->created_at)->format('d/m/Y H:i:s'),
                'fecha_viaje'                    => Carbon::parse($viaje->fecha_viaje)->format('d/m/Y H:i:s'),
                'status'                         => $viaje->status,
                'lugar_destino'                  => $viaje->lugar_destino,


            ];
       }



        if ($viajes) {
          return response()->json([
                    's'                 => 's',
                    'msj'               => 'Historial de Viajes!!',
                    'viajes_activos'    => $viajes_activos
                ], 200);
        }

        return response()->json([
            's'                 => 'n',
            'msj'               => 'No hay viajes!!',
            'viajes_activos'    => $viajes_activos
        ], 200);
    }

    public function buscarCoordenadas($coordenada, $opcion){

        if ($coordenada) {
            $coordenadasArray = explode('--', $coordenada);

            if ($opcion == 'lat')
                return $coordenadasArray[0];
            else
                return $coordenadasArray[1];

        }
        return 0;
    }

    public function obtenerToken($request){

        $return =  explode('Bearer ', $request->header('Authorization'));

        return $return[1];
    }

    public function subir_imagen(Request $request){
        return response()->json([
                    's' => 'n',
                    'msj' => 'Usuario no encontrado!!',
                    'img'   => $request->all()
                ], 405);
    }

    public function cancelar_viaje(Request $request){
        $request->validate([
            'viajes_id'  => 'required'
        ]);

        $token = $this->obtenerToken($request);
        $user = User::where('api_token',  $token)->first();

        if (!$user) {
           return response()->json([
                    's' => 'n',
                    'msj' => 'Usuario no encontrado!!'
                ], 500);
        }

        $viajes = Viajes::where('id', $request->viajes_id)->first();

        $viajes->status = 1;
        $ok = $viajes->save();
        if ($ok ) {
            $datos = ['status' => 1];
            $controller = new FirebaseController;
            $ok = $controller->delete($user->id, $request->viajes_id, 'viajes');
            
            return response()->json([
                    's'                 => 's',
                    'msj'               => 'Viaje cancelado!!'
                ], 201);
        }
        return response()->json([
                    's' => 'n',
                    'msj' => 'Imposible cancelar viaje!!'
                ], 500);
    }

    public function enviar_Mensaje(Request $request){
        $request->validate([
            'viajes_id'  => 'required'
        ]);

        $token = $this->obtenerToken($request);
        $user = User::where('api_token',  $token)->first();

        if (!$user) {
           return response()->json([
                    's' => 'n',
                    'msj' => 'Usuario no encontrado!!'
                ], 405);
        }
        $Chat = [
                    'viaje_id'      => $request->viajes_id,
                    'user_id'       => $user->id,
                    'mensaje'       => $request->mensaje,
                    'created_at'    =>  Carbon::now(),
        ];
        $ChatFire = [
                    'viaje_id'      => $request->viajes_id,
                    'user_id'       => $user->id,
                    'mensaje'       => $request->mensaje,
                    'created_at'    =>  Carbon::now()->format('d/m/Y H:i:s'),
        ]; 



        $controller = new FirebaseController;
        $controller->createChat($ChatFire, 'chat', $request->viajes_id);
        
        $chat = Chat::create($Chat);

        return response()->json([
                    's'                         => 's',
                    'msj'                       => 'Mensaje enviado!!' 
                ], 200);
    }
    public function detectarRegion(Request $request){
        $ip = request()->server('REMOTE_ADDR');
        $arr_ip = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip)); 
        $pais = $this->encontrarPais($arr_ip);
        return response()->json([
                    's'                         => 's',
                    'msj'                       => 'Pais encontrado!!' ,
                    'pais'                      => $pais
                ], 200);
    }

    public function encontrarPais($ip){

        $pais = Pais::where('nombre', $ip["geoplugin_countryName"])->first();
        if ($pais) {
            if (!$pais->code) {
                $pais->code = $ip["geoplugin_countryCode"];
                $pais->save();
            }
            return $pais;
        }

        $pais_nuevo = Pais::create([
            'name'  => $ip["geoplugin_countryName"],
            'code'  => $ip["geoplugin_countryCode"]
        ]);

        return $pais_nuevo;
    }

}
