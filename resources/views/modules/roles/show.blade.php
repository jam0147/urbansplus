@extends('layouts.app')

@section('page_title', __("modules.roles.roles-show") ." ". $role->name)

@section('breadcrumbs')
    {{ Breadcrumbs::render('roles.show', $role) }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title text-uppercase" id="basic-layout-card-center">
                        @lang('modules.roles.roles-show')
                    </h4>
                </div>
                <div class="card-body">
                    <div class="card-block">
                        <form class="form inline">
                            <div class="row">
                                <div class="form-body">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            {{ html()->label(__('modules.roles.roles-form.name')) }}
                                            {{ html()->input('text')->class('form-control')->value($role->display_name)->disabled() }}
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            {{ html()->label(__('modules.roles.roles-form.description')) }}
                                            {{ html()->input('text')->class('form-control')->value($role->description)->disabled() }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" id="permissions-roles">
                                        <thead>
                                          <th width="150px">@lang('modules.permissions.permissions-table.permission')</th>
                                          <th>@lang('modules.permissions.permissions-table.description')</th>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="form-actions center">
                                <a href="{{ route('roles.index') }}" class="btn btn-warning mr-1">
                                    <i class="icon-arrow-left4"></i>
                                    @lang('modules.roles.roles-form.go-back')
                                </a>

                                @can('roles.edit')
                                    <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-primary mr-1">
                                        <i class="icon-edit"></i>
                                        @lang('modules.roles.roles-edit')
                                    </a>
                                @endcan

                                @can('roles.destroy')
                                <button class="btn btn-danger" type="button" onclick="deleteRole({{ $role->id }})">
                                    <i class="icon-trash"></i>
                                    @lang('modules.roles.roles-destroy')
                                </button>
                                @endcan

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#permissions-roles").DataTable({
                processing: true,
                ajax: {
                    url: "{{ route('roles.permissions.data', $role->id) }}",
                    type: "GET"
                },
                columns: [
                    { data: "display_name" },
                    { data: "description" },
                ],
                language: {
                    "emptyTable":     "@lang('datatables.emptyTable')",
                    "info":           "@lang('datatables.info')",
                    "infoEmpty":      "@lang('datatables.infoEmpty')",
                    "infoFiltered":   "@lang('datatables.infoFiltered')",
                    "infoPostFix":    "",
                    "infoThousands":  ",",
                    "lengthMenu":     "@lang('datatables.lengthMenu')",
                    "loadingRecords": "@lang('datatables.loadingRecords')",
                    "processing":     "@lang('datatables.processing')",
                    "search":         "@lang('datatables.search')",
                    "zeroRecords":    "@lang('datatables.zeroRecords')",
                    "paginate": {
                        "first":    "@lang('datatables.paginate.first')",
                        "last":     "@lang('datatables.paginate.last')",
                        "next":     "@lang('datatables.paginate.next')",
                        "previous": "@lang('datatables.paginate.previous')"
                    },
                    "aria": {
                        "sortAscending":  "@lang('datatables.aria.sortAscending')",
                        "sortDescending": "@lang('datatables.aria.sortDescending')"
                    }
                }
            });
        });

        function deleteRole(id) {
            swal({
              title: "@lang('modules.roles.roles-messages.are-you-sure')",
              text: "@lang('modules.roles.roles-messages.text-confirm-delete')",
              icon: 'warning',
              buttons: {
                  cancel: {
                      text: "@lang('modules.roles.roles-messages.cancel')",
                      value: false,
                      visible: true,
                      className: "btn btn-danger mr-1",
                      closeModal: true
                  },
                  confirm: {
                      text: "@lang('modules.roles.roles-messages.confirm-delete')",
                      value: true,
                      visible: true,
                      className: "btn btn-success",
                      closeModal: true
                  }
                },
            }).then((result) => {
                if (result === true) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "DELETE",
                        url: "/admin/roles/" + id,
                        dataType: "JSON",
                    }).success(function (response) {
                        swal({
                            icon: 'success',
                            title: response.title,
                            text: response.message
                        });
                        setTimeout(function (){
                            location.href ="{{ route('roles.index') }}";
                        }, 2000);
                    }).error(function (response) {
                        console.log(response);
                        swal({
                            icon: 'error',
                            title: response.responseJSON.title,
                            text: response.responseJSON.message
                        });
                    });
                } else if (result === false) {
                    swal({
                      title: "@lang('modules.roles.roles-messages.canceled')",
                      text: "@lang('modules.roles.roles-messages.no-deleted')",
                      icon: "error"
                    });
                }
            });
        }
    </script>
@endpush
