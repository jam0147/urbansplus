<?php

namespace App\Helpers;

use App\Helpers\Helpers;
use App\Models\Documentos;
use App\Models\User;
use Hash;
use Image;
use Mail;

trait UsersHelpers
{
    use Helpers;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminDeleteResponse()
    {
        $title = __('modules.users.users-messages.operation-not-allowed');
        $message = __('modules.users.users-messages.not-delete-admin');
        $status = 400;

        return $this->returnJson($title, $message, $status);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function roleAdminAssigned()
    {
        $title = __('modules.users.users-messages.operation-not-allowed');
        $message = __('modules.users.users-messages.no-delete-user-with-role-admin');
        $status = 400;

        return $this->returnJson($title, $message, $status);
    }

    /**
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser($user)
    {
        $user->delete();
        $title = __('modules.users.users-messages.successful-operation');
        $message = __('modules.users.users-messages.user-removed');
        $status = 200;

        return $this->returnJson($title, $message, $status);
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createUser($request)
    {
        $user = User::create([
            'name'   => $request->input('name'),
            'surname'   => $request->input('surname'),
            'dni'   => $request->input('dni'),
            'tlf'   => $request->input('tlf'),
            'pais_id'   => $request->input('pais_id'),
            'observacion'   => $request->input('observacion'),
            'direccion'   => $request->input('direccion'),
            'email'  => $request->input('email'),
            'password' => $request->input('password')
        ]);

        if ($request->hasFile('documents')) {
            $files = $request->file('documents');

            foreach($files as $file) {

                $path = public_path() . '/images/users/' . $user->id ;
                $filename = bcrypt(uniqid()) . $file->getClientOriginalName();
                $file->move($path, $filename);
                $documentos = new Documentos([
                    'user_id'          => $user->id,
                    'tipo_documento_id' => 1,
                    'ruta'              => $filename
                ]);
                $documentos->save();
            }

        }

        $role = $request->input('role');

        $user->assignRole($role);

        return redirect()->back()->with([
            'message' => 'Usuario creado con exito.'
        ]);
    }

    /**
     * @param $user
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser($user, $request)
    {

        $user->update([
            'name'   => $request->input('name'),
            'surname'   => $request->input('surname'),
            'dni'   => $request->input('dni'),
            'tlf'   => $request->input('tlf'),
            'pais_id'   => $request->input('pais_id'),
            'observacion'   => $request->input('observacion'),
            'direccion'   => $request->input('direccion'),
            'email'  => $request->input('email'),
            'password' => $request->input('password')
        ]);

        $user->syncRoles($request->input('role'));

        return redirect()->back()->with([
            'message' => 'Usuario actualizado con exito.'
        ]);
    }
}
