<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    'login' => 'Iniciar sesión',
    'remember-me' => 'Recuérdame',
    'forgot-password' => '¿Olvido su contraseña?',
    'enter-email' => 'Ingresa tu correo electrónico',
    'enter-password' => 'Ingresa tu contraseña',
    'confirm-password' => 'Confirma tu contraseña',
    'send-link' => 'Enviar enlace por correo',
    'login-in-system' => 'Inicia sesión para acceder al sistema.',
    'send-email-description' => 'Se le enviara un correo con el enlace.',
    'reset-password' => 'Restablecer contraseña',
    'unlock-account' => 'Desbloquea tu cuenta',
    'unlock'    => 'Desbloquear',
    'logout' => 'Cerrar sesión',
];
