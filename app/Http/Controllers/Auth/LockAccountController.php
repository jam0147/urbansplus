<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LockAccountController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function lockAccount()
    {
        $user =  Auth::user();

        switch (Auth::check()) {
            case true:
                return $this->lockView($user);
                break;
            case false:
                return redirect()->route('login');
                break;
        }
    }

    /**
     * @param $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function lockView($user)
    {
        session()->put('locked', true);
        return view('auth.unlock', ['user' => $user]);
    }

    /**
     * @param $password
     * @param $user_password
     * @return \Illuminate\Http\RedirectResponse
     */
    private function verifyPassword($password, $user_password)
    {
        $verify_password = Hash::check($password, $user_password);

        switch ($verify_password) {
            case true:
                return $this->unLocked();
                break;
            case false:
                return $this->noUnlocked();
                break;
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    private function emptyPassword()
    {
        return redirect()->back()->with('status', __('validation.unlock.empty-password'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    private function noUnlocked()
    {
        return redirect()->back()->with('status', __('validation.unlock.passwords-not-match'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    private function unLocked()
    {
        session()->forget('locked');
        return redirect()->route('dashboard');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unLockAccount(Request $request)
    {
        $password = $request->input('password');

        $user_password = Auth::user()->password;

        switch ($password) {
            case null:
                return $this->emptyPassword();
                break;
            case isset($password):
                return $this->verifyPassword($password, $user_password);
                break;
        }
    }
}
